################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CLA_SRCS += \
../my_source/driver/cla_task.cla 

C_SRCS += \
../my_source/driver/adc.c \
../my_source/driver/can.c \
../my_source/driver/cap.c \
../my_source/driver/cla.c \
../my_source/driver/cla_data.c \
../my_source/driver/comparator.c \
../my_source/driver/iic.c \
../my_source/driver/pwm.c \
../my_source/driver/sci.c \
../my_source/driver/time.c 

CLA_DEPS += \
./my_source/driver/cla_task.d 

C_DEPS += \
./my_source/driver/adc.d \
./my_source/driver/can.d \
./my_source/driver/cap.d \
./my_source/driver/cla.d \
./my_source/driver/cla_data.d \
./my_source/driver/comparator.d \
./my_source/driver/iic.d \
./my_source/driver/pwm.d \
./my_source/driver/sci.d \
./my_source/driver/time.d 

OBJS += \
./my_source/driver/adc.obj \
./my_source/driver/can.obj \
./my_source/driver/cap.obj \
./my_source/driver/cla.obj \
./my_source/driver/cla_data.obj \
./my_source/driver/cla_task.obj \
./my_source/driver/comparator.obj \
./my_source/driver/iic.obj \
./my_source/driver/pwm.obj \
./my_source/driver/sci.obj \
./my_source/driver/time.obj 

OBJS__QUOTED += \
"my_source\driver\adc.obj" \
"my_source\driver\can.obj" \
"my_source\driver\cap.obj" \
"my_source\driver\cla.obj" \
"my_source\driver\cla_data.obj" \
"my_source\driver\cla_task.obj" \
"my_source\driver\comparator.obj" \
"my_source\driver\iic.obj" \
"my_source\driver\pwm.obj" \
"my_source\driver\sci.obj" \
"my_source\driver\time.obj" 

C_DEPS__QUOTED += \
"my_source\driver\adc.d" \
"my_source\driver\can.d" \
"my_source\driver\cap.d" \
"my_source\driver\cla.d" \
"my_source\driver\cla_data.d" \
"my_source\driver\comparator.d" \
"my_source\driver\iic.d" \
"my_source\driver\pwm.d" \
"my_source\driver\sci.d" \
"my_source\driver\time.d" 

CLA_DEPS__QUOTED += \
"my_source\driver\cla_task.d" 

C_SRCS__QUOTED += \
"../my_source/driver/adc.c" \
"../my_source/driver/can.c" \
"../my_source/driver/cap.c" \
"../my_source/driver/cla.c" \
"../my_source/driver/cla_data.c" \
"../my_source/driver/comparator.c" \
"../my_source/driver/iic.c" \
"../my_source/driver/pwm.c" \
"../my_source/driver/sci.c" \
"../my_source/driver/time.c" 


