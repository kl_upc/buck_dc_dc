################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../my_source/can_protocol.c \
../my_source/main.c \
../my_source/main_isr.c \
../my_source/sys_data.c \
../my_source/sys_task.c 

C_DEPS += \
./my_source/can_protocol.d \
./my_source/main.d \
./my_source/main_isr.d \
./my_source/sys_data.d \
./my_source/sys_task.d 

OBJS += \
./my_source/can_protocol.obj \
./my_source/main.obj \
./my_source/main_isr.obj \
./my_source/sys_data.obj \
./my_source/sys_task.obj 

OBJS__QUOTED += \
"my_source\can_protocol.obj" \
"my_source\main.obj" \
"my_source\main_isr.obj" \
"my_source\sys_data.obj" \
"my_source\sys_task.obj" 

C_DEPS__QUOTED += \
"my_source\can_protocol.d" \
"my_source\main.d" \
"my_source\main_isr.d" \
"my_source\sys_data.d" \
"my_source\sys_task.d" 

C_SRCS__QUOTED += \
"../my_source/can_protocol.c" \
"../my_source/main.c" \
"../my_source/main_isr.c" \
"../my_source/sys_data.c" \
"../my_source/sys_task.c" 


