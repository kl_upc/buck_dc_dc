/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_03_30;
 * Edition:                     1.0;
 * Summary:                     MAIN_ISR
 * .............................................
 */

/* FILE INCLUDE */
#include "main_isr.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */

/* FUNCTION */
#pragma CODE_SECTION(main_isr, "ramfuncs")
#pragma CODE_SECTION(emerge_protect_isr, "ramfuncs")

/*
 *Fuction:                      main_isr
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          the Main_isr Funtion
 */
__interrupt void  main_isr(void)
{


    Cla1ForceTask1andWait();                 //启动CLA运行
    //串口查询
//    if((SciaRegs.SCIRXST.bit.RXRDY) && (GpioDataRegs.GPADAT.bit.GPIO18 == 0))
    if(SciaRegs.SCIRXST.bit.RXRDY)
    {
        uart_r = SciaRegs.SCIRXBUF.bit.RXDT;
    }
    AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;     //Clear ADCINT1 flag reinitialize for next SOC
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

    return;
}

__interrupt void  emerge_protect_isr(void)
{
//    DisablePwm();
}
