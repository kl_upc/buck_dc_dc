/*
 * .............................................
 * Author:                      ;
 * Company:                     TOPSCOMM;
 * Data:                        2019_05_23;
 * Edition:                     1.0;
 * Summary:                     sys_task
 * .............................................
 */
/* FILE INCLUDE */
#include "driver.h"
#include <string.h>

/* DEFINE SOME PARAMETERS */


//#define SET_VOLTAGE     637000
//#define SET_CURRENT     20000
//#define CHANGE_VOLTAGE  2369
//#define CHANGE_CURRENT  685
//#define CHANGE_POWER    500
//#define MOST_VOLTAGE    600000
//#define MOST_CURRENT    9000

enum state  sys_state = SYS_INIT;

/* DEFINE SOME VARIABLES */


/*
 *Fuction:                      sys_task
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *
 */
/* FUNCTION */
void SysTask(void)
{
    int clr_num;
    switch(sys_state)
    {
        case SYS_INIT:
        {
            sys_init();
//            DisablePwm(); //关闭PWM
//            sys_run.sys_set.value.set_voltage = 0x00000000;
//            sys_run.sys_set.value.set_current = 0x00000000;
            for( clr_num = 0; clr_num < 10;clr_num++)
            {
                //清除锁存故障
                EnableFaultClrOut();
                DELAY_US(1000);
                //使能故障锁存
                DisableFaultClrOut();
                // 清除错误区标志
                EPwm1Regs.TZCLR.all = 0xFF;
                EPwm2Regs.TZCLR.all = 0xFF;
                send_fault = 0;
            }
//            EnableDischarge() ; //放电
            run_parameter.start_state = 0;
            sys_state = SYS_CHECK;
        }
        break;
        case SYS_CHECK:
        {
            run_parameter.start_state = 0;
            if(sys_run.sys_set.value.set_on_off == 1) //又开机，清除故障一次
            {
                for( clr_num = 0; clr_num < 10;clr_num++)
                {
                    //清除锁存故障
                    EnableFaultClrOut();
                    DELAY_US(1000);
                    //使能故障锁存
                    DisableFaultClrOut();
                    // 清除错误区标志
                    EPwm1Regs.TZCLR.all = 0xFF;
                    EPwm2Regs.TZCLR.all = 0xFF;
                    send_fault = 0;
                }
            }
            //系统检测
            if(sys_check())           //运行系统检测函数。
            {
                sys_state = SYS_WAIT_OPEN;
            }
            else
            {
                sys_state = SYS_ERROR;
            }
        }
        break;
        case SYS_WAIT_OPEN: //开机，合强电，清故障
        {
            run_parameter.start_state = 0;
//            if((Cla_comm_data.s1 == 0)&&(run_parameter.s2 == 0)&&(run_parameter.s3 == 0)) //正常关机，且都未在运行
//            {
//                EnableDischarge();
//            }
//            DELAY_US(1000);
//            DisableDischarge();
            if(sys_run.sys_set.value.set_on_off == 1)
            {
                EnableChargOut(); //充电
                if(result.LV3 > (result.HVBUS - 5)) //电压充到母线电压-5以上则充电结束
                {
                    DELAY_US(1000000);//1s延时1秒关闭主开关
                    EnablePowerOut(); //吸合主继电器
                    run_parameter.Fan_state = 1;
                    EnableFan();
                    for(clr_num = 0; clr_num < 10;clr_num++)
                    {
                        //清除锁存故障
                        EnableFaultClrOut();
                        DELAY_US(1000);
                        //使能故障锁存
                        DisableFaultClrOut();
                        // 清除错误区标志
                        EPwm1Regs.TZCLR.all = 0xFF;
                        EPwm2Regs.TZCLR.all = 0xFF;
                        send_fault = 0;
                    }
                    //              EnableDischarge() ; //放电
                    if(sys_check())           //运行系统检测函数。
                    {
                        sys_state = SYS_OPEN;
                    }
                    else
                    {
                        sys_state = SYS_ERROR;
                    }

                }
            }
            else
            {
                sys_state = SYS_WAIT_OPEN;
            }
        }
        break;
        case  SYS_OPEN: //进入start模式
        {
            DisableDischarge(); //不放电
            if(sys_run.sys_set.value.set_on_off == 0)
            {
                sys_state = SYS_CLOSE;
            }
            if(sys_run.sys_set.value.set_voltage >= 10000)// && (sys_run.sys_set.value.set_voltage < SET_VOLTAGE))
            {
                run_parameter.state = 2;

//                sys_run.sys_set.value.set_voltage = 0x00000000;
                sys_state = SYS_VOLTAGE_MODE;
            }
//            if(sys_run.sys_set.value.set_current >= 1000)// && (sys_run.sys_set.value.set_current < SET_CURRENT))
//            {
//                run_parameter.state = 1;
        //            sys_run.sys_set.value.set_voltage = 0x00000000; //清零
//                sys_state = SYS_CURRENT_MODE;
//            }
        }
        break;
//        case SYS_SELECT_MODE:
//        {
//            if(sys_run.sys_set.value.set_on_off == 0)
//            {
//                sys_state = SYS_CLOSE;
//            }
//            if(sys_run.sys_set.value.set_current != 0x00000000)// && (sys_run.sys_set.value.set_current < SET_CURRENT))
//            {
//
//                run_parameter.state = 1;
//                DisableDischarge(); //不放电
//                sys_run.sys_set.value.set_voltage = 0x00000000; //清零
//                sys_state = SYS_CURRENT_MODE;
//            }
//            else if(sys_run.sys_set.value.set_voltage != 0x00000000)// && (sys_run.sys_set.value.set_voltage < SET_VOLTAGE))
//            {
//                run_parameter.state = 2;
//                DisableDischarge(); //不放电
//                sys_run.sys_set.value.set_voltage = 0x00000000;
//                sys_state = SYS_VOLTAGE_MODE;
//            }
//        }
//        break;
        case SYS_CURRENT_MODE:
        {
            DisableDischarge(); //不放电
            run_parameter.start_state = 1;
            EnablePwm();
            if(sys_check() ==0)
            {
                DisablePwm();
                run_parameter.state = 0;
                run_parameter.start_state = 0;
                sys_state = SYS_ERROR;
            }
            if(sys_run.sys_set.value.set_on_off == 0)
            {
                DisablePwm();
                run_parameter.state = 0;
                run_parameter.start_state = 0;
                sys_state = SYS_CLOSE;
            }

//            if(sys_run.sys_value.value.OutVolt > CHANGE_VOLTAGE)
//            {
//                sys_run.sys_set.value.set_voltage = MOST_VOLTAGE;
//            }
//            if((sys_run.sys_set.value.set_voltage != 0x00000000) && (sys_run.sys_set.value.set_voltage < SET_VOLTAGE))

//            else
//            {
//                sys_state = SYS_CURRENT_MODE;


                if(sys_run.sys_set.value.set_current >= 100)
                {
                    run_parameter.set_i = sys_run.sys_set.value.set_current * 0.001;
                    sys_run.sys_set.value.set_current = 0x00000000; //清零
                }
                if(sys_run.sys_set.value.set_voltage >= 10000)
                {
                    run_parameter.state = 2;
                    sys_state = SYS_VOLTAGE_MODE;
                }
        }
        break;
        case SYS_VOLTAGE_MODE:
        {
              DisableDischarge(); //不放电
              run_parameter.start_state = 1;
              EnablePwm();
              if(sys_check() ==0)
             {
                 DisablePwm();
                 run_parameter.state = 0;
                 run_parameter.start_state = 0;
                 sys_state = SYS_ERROR;
             }
//            if(sys_run.sys_value.value.DcCurrtO > CHANGE_CURRENT)
//            {
//                sys_run.sys_set.value.set_current = MOST_CURRENT;
//            }
//            if((sys_run.sys_set.value.set_current != 0x00000000) && (sys_run.sys_set.value.set_current < SET_CURRENT))
//            {
//                sys_run.sys_set.value.set_voltage = 0x00000000;
//                sys_state = SYS_CURRENT_MODE;
//            }
//            else
//            {
//                sys_state = SYS_VOLTAGE_MODE;
                if(sys_run.sys_set.value.set_on_off == 0) //关机
                {
                    DisablePwm();
                    run_parameter.state = 0;
                    run_parameter.start_state = 0;
                    sys_state = SYS_CLOSE;
                }
                if(sys_run.sys_set.value.set_voltage >= 10000)
                {
                    run_parameter.set_u = sys_run.sys_set.value.set_voltage * 0.001;
                    sys_run.sys_set.value.set_voltage = 0x00000000; //清零
                }
                if(sys_run.sys_set.value.set_current >= 100)
                {
                    run_parameter.state = 1;
                    sys_state = SYS_CURRENT_MODE;
                }
        }
        break;
        case SYS_CLOSE: //正常关机
        {
            DisablePwm();
//            DELAY_US(30);
//            sys_run.sys_set.value.set_voltage = 0x00000000;
//            sys_run.sys_set.value.set_current = 0x00000000;
            run_parameter.state = 0;
            run_parameter.start_state = 0;
            sys_run.sys_set.value.set_current = 0x00000000;
            sys_run.sys_set.value.set_voltage = 0x00000000;
//            run_parameter.set_val = 0;
            DisableChargOut(); //关闭充电
            DELAY_US(50);
//            DisableFan();
            DisablePowerOut(); //关闭主接触器
//            EnableDischarge() ; //放电
            sys_state = SYS_WAIT_OPEN;
        }
        break;
        case SYS_ERROR: //非正常关机
        {
            DisableDischarge();//不放电
            DisablePwm();
            sys_run.sys_set.value.set_on_off = 0; //关机
            run_parameter.state = 0;
            run_parameter.start_state = 0;
            sys_run.sys_set.value.set_voltage = 0x00000000;
            sys_run.sys_set.value.set_current = 0x00000000;
            //故障则关闭继电器
            DisableChargOut(); //关闭充电
            DELAY_US(100);
            DisablePowerOut(); //关闭主接触器
            //故障标志置位
            sys_state = SYS_CHECK;
        }
        break;
        default:sys_state = SYS_INIT;
        break;
    }
}

/*
 *Fuction:                      sys_check
 *Parameter:                    void
 *Return:                       sys_check_result
 *Summary:
 *
 */
int sys_check(void)
{
    if((result.CURR1 > 50)|| (result.CURR1 < -50))//输入过流
    {
        send_fault = send_fault | 0x1;
    }
    if((result.CURR2 > 50)|| (result.CURR2 < -50)) //输出1过流
    {
        send_fault = send_fault | 0x10;
    }
    if((result.CURR4 > 50)|| (result.CURR4 < -50)) //输出2过流
    {
        send_fault = send_fault | 0x100;
    }
    if(result.HVPORT > 740) //输出过压
    {
        send_fault = send_fault | 0x1000;
    }
//    if((result.TMP1 > 50) ||(result.TMP2 > 50)) //过温
//    {
//        send_fault = send_fault | 0x10000;
//    }
    if(EPwm1Regs.TZFLG.bit.OST != 0) //fault故障，低
    {
        send_fault = send_fault | 0x100000;
    }
    if(EPwm1Regs.TZFLG.bit.DCAEVT2 != 0) //输出1过流
    {
        send_fault = send_fault | 0x1000000;
    }
    if(EPwm1Regs.TZFLG.bit.DCAEVT1 != 0) //输出2过流
    {
        send_fault = send_fault | 0x10000000;
    }
    if(send_fault > 0) //有故障
    {
        DisablePwm(); //关闭PWM
        run_parameter.start_state = 0;
//        send485(send_fault);

//        send_fault = 0;
//        EnableLATCHEN(); //锁存故障
        return 0;
    }
    else
    {
//        DisableLATCHEN(); //恢复锁存
        return 1;
    }
//    return 1;
//    if(GpioDataRegs.GPADAT.bit.GPIO24 == 0) //电压过低停止风机
//    {
//        DisableFan();
//    }
}
/*
 *Fuction:                      sys_init
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *
 */
void sys_init(void)
{
    //系统初始化，初始化状态变量
    sys_run.sys_can_state.averge_current_report = 0;
    sys_run.sys_can_state.host_need_report_data = 0;
    sys_run.sys_can_state.host_need_report_mode = 0;
    sys_run.sys_can_state.host_need_report_onff = 0;
    sys_run.sys_emerg_fault = 0;
    sys_run.sys_soft_fault = 0;
    //    memset(sys_run.sys_set.set_buffer,0,3*sizeof(Uint32));
    sys_run.sys_set.value.set_voltage = 0;
    sys_run.sys_set.value.set_current = 0;
    sys_run.sys_set.value.set_on_off = 0;
//    memset(sys_run.sys_average_current,0,10*sizeof(Uint32));
    sys_run.sys_limit.value.BusVol_Limit = 0;
    sys_run.sys_limit.value.BusVoltON_Limit = 0;
    sys_run.sys_limit.value.BusVoltPN_Limit = 0;
    sys_run.sys_limit.value.DcCurrtI_Limit = 0;
    sys_run.sys_limit.value.DcCurrtO_Limit = 0;
    sys_run.sys_limit.value.OutVolt_Limit = 0;
    memset(sys_run.sys_value.value_buffer,0,6*sizeof(Uint32));

    //读取EEPROM 存储参数。
//    eeprom_iic_read_page();
    //将CLA中参数赋值。
    return ;
}
