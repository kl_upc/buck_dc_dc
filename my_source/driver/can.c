/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_01;
 * Edition:                     1.0;
 * Summary:                     CAN_FUNCTION
 * .............................................
 */
/* FILE INCLUDE */
//#include "can.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */
struct ECAN_REGS ECanaShadow;
/* FUNCTION */


/*
 *Fuction:                      init_can
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the CAN
 */
void InitCan(void)
{
    InitECanGpio(); //gpio
    InitECan();    //1M BPS

}

void InitCanbox(void)
{
    EALLOW;
    ECanaMboxes.MBOX0.MSGID.all =  (0x9207C080 | sys_run.sys_id.id_buffer);           //config as transmit mailbox
    ECanaMboxes.MBOX1.MSGID.all =  (0x92078080 | sys_run.sys_id.id_buffer);           //config as transmit mailbox
    ECanaMboxes.MBOX15.MSGID.all = 0xD2078080;           //config as receive mailbox
    ECanaLAMRegs.LAM15.all = 0x10000007;                 //set mask
    ECanaMboxes.MBOX16.MSGID.all = 0xD307C080;           //config as receive mailbox
    ECanaLAMRegs.LAM16.all = 0x10000007;                 //set mask

    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;
    ECanaShadow.CANMD.bit.MD0 = 0;
    ECanaShadow.CANMD.bit.MD1 = 0;
    ECanaShadow.CANMD.bit.MD15 = 1;
    ECanaShadow.CANMD.bit.MD16 = 1;
    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;
    //设置数据长度8字节
    ECanaMboxes.MBOX0.MSGCTRL.bit.DLC = 8;
    ECanaMboxes.MBOX1.MSGCTRL.bit.DLC = 8;
    //无远程帧请求
    ECanaMboxes.MBOX0.MSGCTRL.bit.RTR = 0;
    ECanaMboxes.MBOX1.MSGCTRL.bit.RTR = 0;
    //使能邮箱
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME0 = 1;
    ECanaShadow.CANME.bit.ME1 = 1;
    ECanaShadow.CANME.bit.ME15 = 1;
    ECanaShadow.CANME.bit.ME16 = 1;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;

//    ECanaRegs.CANMIM.bit.MIM0 = 1;
//    ECanaRegs.CANMIM.bit.MIM1 = 1;
//    ECanaRegs.CANMIM.bit.MIM15 = 1;
//    ECanaRegs.CANMIM.bit.MIM16 = 1;

    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.STM = 0;                   //1--回环自测模式
    ECanaShadow.CANMC.bit.ABO = 1;                 // 识别到总线自动恢复
    ECanaShadow.CANMC.bit.WUBA = 1;
//	ECanaShadow.CANMC.bit.SUSP = 1;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
    EDIS;
}
