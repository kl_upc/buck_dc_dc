/*
 * .............................................
 * Author:
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_23;
 * Edition:                     1.0;
 * Summary:                     CLA_DATA
 * .............................................
 */
/* FILE INCLUDE */
//#include "cla_data.h"
#include "driver.h"
#include XSTRINGIZE(XCONCAT(cla,_data.h))
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */

#pragma DATA_SECTION(IPICtl1,"Cla1DataRam1");
PI IPICtl1   = {0, 0, 0,
                2.0, 0.005, 0, 0, 0,
                100.0, -100.0, 0};
#pragma DATA_SECTION(IPICtl2,"Cla1DataRam1");
PI IPICtl2   = {0, 0, 0,
                2.0, 0.005, 0, 0, 0,
                100.0, -100.0, 0};
#pragma DATA_SECTION(IPICtlcom,"Cla1DataRam1");
PI IPICtlcom = {0, 0, 0,
                0.2, 0.005, 0, 0, 0,
                100.0, -100.0, 0};
#pragma DATA_SECTION(UPICtl,"Cla1DataRam1");
PI UPICtl    = {0, 0, 0,
                0.3, 0.001, 0, 0, 0,
                200.0, -200.0,  0};
#pragma DATA_SECTION(FAN,"Cla1DataRam1");
PI FAN       = {0, 0, 0,
                1.2, 0.001, 0, 0, 0,
                100.0, -100.0, 0};
#pragma DATA_SECTION(offset,"CpuToCla1MsgRAM");
ad_offset  offset = {0,0,0,0,0,0};
#pragma DATA_SECTION(result,"Cla1ToCpuMsgRAM");
ad_result result = {0,0,0,0,0,0,0,0,0,0,0,0,0};

#pragma DATA_SECTION(ctrl1,"Cla1DataRam1");
control ctrl1 = {0,0,0,0};
#pragma DATA_SECTION(ctrl2,"Cla1DataRam1");
control ctrl2 = {0,0,0,0};
#pragma DATA_SECTION(ctrl_fan,"Cla1DataRam1");
control_fan ctrl_fan = {0, 0, 0, 0};
#pragma DATA_SECTION(ModChange,"Cla1DataRam1");
Uint16   ModChange = 0;
#pragma DATA_SECTION(UPICtl_temp,"Cla1DataRam1");
float UPICtl_temp = 0;
#pragma DATA_SECTION(run_parameter,"CpuToCla1MsgRAM");
cla_run  run_parameter = {
                          .start_state = 0,
                          .Fan_state = 0,
                          .state = 0,
                          .set_i = 1,
                          .set_u = 10,
                          .set_fan_temp = 40,
                          .fault_flag = 0,
                          .pi_kp_test = 2.0,
                          .pi_ki_test = 0.005,
                          .Upi_kp_test = 0.3,
                          .Upi_ki_test = 0.001,
                          .IOutMax_test = 100.0,
                          .IOutMin_test = -100.0,
                          .s2 = 0,
                          .s3 = 0,
                          .i2sum = 0.0,
                          .i3sum = 0.0
};
#pragma DATA_SECTION(Cla_comm_data,"Cla1ToCpuMsgRAM");
cla_comm_data Cla_comm_data = {0, 0};

#pragma DATA_SECTION(I_delaytime,"Cla1DataRam1");
Uint16  I_delaytime = 0;
#pragma DATA_SECTION(sys_id,"CpuToCla1MsgRAM");
Uint32   sys_id;
#pragma DATA_SECTION(cla_test1,"Cla1ToCpuMsgRAM");
#pragma DATA_SECTION(cla_test2,"Cla1ToCpuMsgRAM");
#pragma DATA_SECTION(cla_test3,"Cla1ToCpuMsgRAM");
#pragma DATA_SECTION(cla_test4,"Cla1ToCpuMsgRAM");

Uint32 cla_test1 = 0;
Uint32 cla_test2 = 0;
float32 cla_test3 = 0;
float32 cla_test4 = 0;

