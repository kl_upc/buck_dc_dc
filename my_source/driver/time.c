/*
 * .............................................
 * Author:
 * Company:                     TOPSCOMM;
 * Data:                        2019_07_01;
 * Edition:                     1.0;
 * Summary:                     TIME
 * .............................................
 */
/* FILE INCLUDE */
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */

/* FUNCTION */


/*
 *Fuction:                      init_time
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the TIMER
 */
void InitTime(void)
{
    EALLOW;
    PieVectTable.TINT1 = &time_isr; //cpu1定时器
    EDIS;
    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer1, 60, 20000); //20ms
    CpuTimer1Regs.TCR.all = 0x4000;
    IER |= M_INT13;
    return;
}
/*
 *Fuction:                      time_isr
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          time interrupt service
 */
__interrupt void time_isr(void)
{
    //发送均流值
    sys_run.sys_can_state.averge_current_report = 1;
//    Cla1ForceTask2andWait(); //风机控制

}
