/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_03_30;
 * Edition:                     1.0;
 * Summary:                     CLA_FUNCTION
 * .............................................
 */


/* FILE INCLUDE */

#include "string.h"
//#include "cla.h"
//#include "cla_task.h"
//#include "cla_data.h"
#include "driver.h"
#include XSTRINGIZE(XCONCAT(cla,_data.h))
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */
extern Uint32 Cla1funcsRunStart, Cla1funcsLoadStart, Cla1funcsLoadSize;
extern Uint32 Cla1Prog_Start;
extern Uint16 Cla1mathTablesLoadStart;
extern Uint16 Cla1mathTablesRunStart;
extern Uint16 Cla1mathTablesLoadSize;
/* FUNCTION */

/*
 *Fuction:                      init_cla
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          the init_cla Funtion
 */
void InitCla(void)
{
    memcpy((Uint32 *)&Cla1funcsRunStart, (Uint32 *)&Cla1funcsLoadStart,
               (Uint32)&Cla1funcsLoadSize);
    memcpy(&Cla1mathTablesRunStart, &Cla1mathTablesLoadStart, (Uint32)&Cla1mathTablesLoadSize);
    EALLOW;
    Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT2 = (Uint16)((Uint32)&ClaTask2 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT3 = (Uint16)((Uint32)&ClaTask3 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT4 = (Uint16)((Uint32)&ClaTask4 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT5 = (Uint16)((Uint32)&ClaTask5 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT6 = (Uint16)((Uint32)&ClaTask6 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT7 = (Uint16)((Uint32)&ClaTask7 - (Uint32)&Cla1Prog_Start);
    Cla1Regs.MVECT8 = (Uint16)((Uint32)&ClaTask8 - (Uint32)&Cla1Prog_Start);
    EDIS;

    EALLOW;
    Cla1Regs.MPISRCSEL1.bit.PERINT1SEL = CLA_INT1_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT2SEL = CLA_INT2_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT3SEL = CLA_INT3_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT4SEL = CLA_INT4_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT5SEL = CLA_INT5_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT6SEL = CLA_INT6_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT7SEL = CLA_INT7_NONE;
    Cla1Regs.MPISRCSEL1.bit.PERINT8SEL = CLA_INT8_NONE;

    Cla1Regs.MIER.all                  = 0x00FF;
    EDIS;

    EALLOW;
    Cla1Regs.MMEMCFG.bit.PROGE = 1;
    Cla1Regs.MCTL.bit.IACKE = 1;
    Cla1Regs.MMEMCFG.bit.RAM0E  = 1;
    Cla1Regs.MMEMCFG.bit.RAM1E  = 1;
    EDIS;
}
