/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_22;
 * Edition:                     1.0;
 * Summary:                     COMPARATOR_FUNCTION
 * .............................................
 */


/* FILE INCLUDE */
//#include "comparator.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */

/* FUNCTION */

/*
 *Fuction:                      init_comparator
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the COMPARATOR
 */
void InitComparator(void)
{
    InitCompGpio();
    EALLOW;
    //(3.37x/1023-1.48)*8*100/4 = 电流保护值
    //输出2
    Comp1Regs.DACCTL.bit.DACSOURCE = 0; //DAC controlled by DACVA
    Comp1Regs.DACVAL.bit.DACVAL = 525; //255*(3.3 -0)/1023 = 0.823
    Comp1Regs.COMPCTL.bit.COMPSOURCE = 0; //连接内部DAC
    Comp1Regs.COMPCTL.bit.SYNCSEL = 0; //同步
    Comp1Regs.COMPCTL.bit.CMPINV = 0;  //同向输出
    Comp1Regs.COMPCTL.bit.COMPDACEN = 1; //使能

    //输出1
    Comp2Regs.DACCTL.bit.DACSOURCE = 0;
    Comp2Regs.DACVAL.bit.DACVAL = 525; //50A
    Comp2Regs.COMPCTL.bit.COMPSOURCE = 0;
    Comp2Regs.COMPCTL.bit.SYNCSEL = 0;
    Comp2Regs.COMPCTL.bit.CMPINV = 0;
    Comp2Regs.COMPCTL.bit.COMPDACEN = 1;
    //输入总
    Comp3Regs.DACCTL.bit.DACSOURCE = 0;
    Comp3Regs.DACVAL.bit.DACVAL = 525; //50A
    Comp3Regs.COMPCTL.bit.COMPSOURCE = 0;
    Comp3Regs.COMPCTL.bit.SYNCSEL = 0;
    Comp3Regs.COMPCTL.bit.CMPINV = 0;
    Comp3Regs.COMPCTL.bit.COMPDACEN = 1;
    EDIS;
}
