/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_18;
 * Edition:                     1.0;
 * Summary:                     SCI_FUNCTION
 * .............................................
 */

/* FILE INCLUDE */
//#include "sci.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */

/* FUNCTION */

/*
 *Fuction:                      init_sci
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the SCI
 */
void InitSciNew(void)
{
    InitSciGpio();

//    EALLOW;
//    PieVectTable.SCIRXINTA = &sci_receive;
//    PieVectTable.SCITXINTA = &sci_transmit;
//    EDIS;

    SciaRegs.SCICCR.all =0x0007;// 1 stop bit,  No loopback,No parity,8 char bits,async mode, idle-line protocol
    SciaRegs.SCICTL1.all =0x0003; // enable TX, RX, internal SCICLK,Disable RX ERR, SLEEP, TXWAKE

//    SciaRegs.SCICTL2.all =0x0003;
//    SciaRegs.SCICTL2.bit.TXINTENA =1;
//    SciaRegs.SCICTL2.bit.RXBKINTENA =1;

    SciaRegs.SCIHBAUD = 0x0000;
    SciaRegs.SCILBAUD = 0x00C2;//波特率9600
//    SciaRegs.SCILBAUD = 0x0061; //波特率19200

//    SciaRegs.SCICCR.bit.LOOPBKENA =1; // Enable loop back
//    SciaRegs.SCIFFTX.all=0xC021;
//    SciaRegs.SCIFFRX.all=0x0021;
//    SciaRegs.SCIFFCT.all=0x00;

    SciaRegs.SCICTL1.bit.SWRESET = 1; //重启SCI
//    SciaRegs.SCIFFTX.bit.TXFIFOXRESET=1;
//    SciaRegs.SCIFFRX.bit.RXFIFORESET=1;

//    PieCtrlRegs.PIEIER9.bit.INTx1 = 1;
//    IER|=M_INT9;
}

//__interrupt void sci_receive(void)
//{
//    uart_r = SciaRegs.SCIRXBUF.bit.RXDT;
//
////    Enable485Send();
////    DELAY_US(200);
////
////    while(SciaRegs.SCICTL2.bit.TXRDY == 1)
////    {
////        SciaRegs.SCITXBUF = uart_r;
////    }
////    DELAY_US(5000);
////    Enable485Re(); //使能接收
//    SciaRegs.SCIFFRX.bit.RXFIFORESET = 0;
//    SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
//    SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1;
//    SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;
//    PieCtrlRegs.PIEACK.bit.ACK9 = 1;
//
//
//}

//__interrupt void sci_transmit(void)
//{
//
//}
