/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_18;
 * Edition:                     1.0;
 * Summary:                     SCI_FUNCTION
 * .............................................
 */

/* FILE INCLUDE */
//#include "iic.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */


/* DEFINE SOME VARIABLES */

/* FUNCTION */

/*
 *Fuction:                      init_IIC
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the IIC
 */
void init_iic(void)
{
    InitI2CGpio();
    I2caRegs.I2CSAR = 0x0050;
    I2caRegs.I2CPSC.all = 6;
    I2caRegs.I2CCLKL = 10;
    I2caRegs.I2CCLKH = 5;
    I2caRegs.I2CIER.all = 0x00; // Disable SCD & ARDY interrupts
    I2caRegs.I2CMDR.all = 0x0020;
    I2caRegs.I2CFFTX.all = 0x6000;  // Enable FIFO mode and TXFIFO
    I2caRegs.I2CFFRX.all = 0x2040;  // Enable RXFIFO, clear RXFFINT
}
/*
 *Fuction:                      eeprom_iic_write_byte
 *Parameter:                    Uint16 addr,Uint16 data
 *Return:                       void
 *Summary:
 *          write a byte to eeprom
 */
void eeprom_iic_write_byte(Uint16 addr,Uint16 data)
{
    while((I2caRegs.I2CMDR.bit.STP) | (I2caRegs.I2CSTR.bit.BB))
    {
        DELAY_US(100);
    }
    I2caRegs.I2CCNT = 4;
    I2caRegs.I2CDXR = (addr >> 8) & 0xFF;
    I2caRegs.I2CDXR = (addr & 0xFF);
    I2caRegs.I2CDXR = (data & 0xFF);
    I2caRegs.I2CDXR = (data >> 8) & 0xFF;
    I2caRegs.I2CMDR.all = 0x6E20;
}
/*
 *Fuction:                      eeprom_iic_write_page
 *Parameter:                    Uint16 addr,Uint16 *buf,Uint16 len
 *Return:                       len
 *Summary:
 *          write a page of eeprom
 */
Uint16 eeprom_iic_write_page(Uint16 addr,Uint16 *buf,Uint16 len)
{
    Uint16 i;
    i = (((addr >> 7) + 1) << 7) - addr;    /*��ҳʣ�೤��*/
    if((len << 1) > i)
    {
        len = i >> 1;
    }
    if(len == 0)
    {
        return 0;
    }
    while((I2caRegs.I2CMDR.bit.STP) | (I2caRegs.I2CSTR.bit.BB))
    {
        DELAY_US(100);
    }
    I2caRegs.I2CDXR = (addr >> 8) & 0xFF;
    I2caRegs.I2CDXR = (addr & 0xFF);

    I2caRegs.I2CMDR.all = 0x66A0;

    for(i = 0; i < len; i++)
    {
        I2caRegs.I2CDXR = (*(buf + i)) & 0xFF;
        while(I2caRegs.I2CSTR.bit.ARDY != 1)
        {
            ;
        }
        I2caRegs.I2CDXR = (*(buf + i)) >> 8;
        while(I2caRegs.I2CSTR.bit.ARDY != 1)
        {
            ;
        }
    }
    while(I2caRegs.I2CSTR.bit.ARDY != 1)
    {
        ;
    }
    I2caRegs.I2CMDR.bit.STP = 1;
    return len;
}

/*
 *Fuction:                      eeprom_iic_write_section
 *Parameter:                    Uint16 addr, Uint16 * buf, Uint16 len
 *Return:                       void
 *Summary:
 *          write a section of eeprom
 */
void eeprom_iic_write_section(Uint16 addr, Uint16 * buf, Uint16 len)
{
    Uint16 tmp;

    addr = addr & 0xFFFE;

    if(((Uint32)(len) << 1) + addr > 65535)
    {
        len = (65535 - addr) >> 1;
    }

    while(len)
    {
        tmp = eeprom_iic_write_page(addr, buf, len);
        addr += (tmp << 1);
        buf += tmp;
        len -= tmp;
        DELAY_US(5000);
    }
}
/*
 *Fuction:                      eeprom_iic_read_byte
 *Parameter:                    Uint16 addr
 *Return:                       data
 *Summary:
 *          read a byte from eeprom
 */
Uint16 eeprom_iic_read_byte(Uint16 addr)
{
    Uint16 tmp;
    while((I2caRegs.I2CMDR.bit.STP) | (I2caRegs.I2CSTR.bit.BB))
    {
        DELAY_US(100);
    }

    I2caRegs.I2CCNT = 2;
    I2caRegs.I2CDXR = (addr >> 8) & 0xFF;
    I2caRegs.I2CDXR = (addr & 0xFF);
    I2caRegs.I2CMDR.all = 0x2620;

    while(I2caRegs.I2CSTR.bit.ARDY != 1)
    {
        DELAY_US(100);
    }
    I2caRegs.I2CCNT = 2;
    I2caRegs.I2CMDR.all = 0x2C20;
    while(I2caRegs.I2CSTR.bit.SCD != 1)
    {
        DELAY_US(100);
    }
    tmp = I2caRegs.I2CDRR;
    tmp = tmp  + (I2caRegs.I2CDRR << 8);
    return tmp;
}
/*
 *Fuction:                      eeprom_iic_read_page
 *Parameter:                    Uint16 addr, Uint16 * buf, Uint16 len
 *Return:                       len
 *Summary:
 *          read a page from eeprom
 */
Uint16 eeprom_iic_read_page(Uint16 addr, Uint16 * buf, Uint16 len)
{
    Uint16 i;
    Uint16 tmp;

    if(len == 0)
    {
        return 0;
    }
    if(len > 7)
    {
        len = 7;
    }

    while((I2caRegs.I2CMDR.bit.STP) | (I2caRegs.I2CSTR.bit.BB))
    {
        DELAY_US(100);
    }
    I2caRegs.I2CCNT = 2;
    I2caRegs.I2CDXR = (addr >> 8) & 0xFF;
    I2caRegs.I2CDXR = (addr & 0xFF);
    I2caRegs.I2CMDR.all = 0x2620;   /*Without Stop Bit*/

    while(I2caRegs.I2CSTR.bit.ARDY != 1)
    {
        ;
    }
    I2caRegs.I2CCNT = (len << 1);
    I2caRegs.I2CMDR.all = 0x2C20;

    while(I2caRegs.I2CSTR.bit.SCD != 1)
    {
        DELAY_US(100);
    }
    for(i = 0; i < len; i++)
    {
        tmp = I2caRegs.I2CDRR;
        tmp = tmp + (I2caRegs.I2CDRR << 8);
        *(buf + i) = tmp;
    }
    return len;
}
/*
 *Fuction:                      eeprom_iic_read_section
 *Parameter:                    Uint16 addr
 *Return:                       data
 *Summary:
 *          read a section from eeprom
 */
void eeprom_iic_read_section(Uint16 addr, Uint16 * buf, Uint16 len)
{
    Uint16 tmp;

    addr = addr & 0xFFFE;

    if(((Uint32)(len) << 1) + addr > 65535)
    {
        len = (65535 - addr) >> 1;
    }
    while(len)
    {
        tmp = eeprom_iic_read_page(addr, buf, len);
        addr += (tmp << 1);
        buf += tmp;
        len -= tmp;
        DELAY_US(100);
    }
}
