/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_03_27;
 * Edition:                     1.0;
 * Summary:                     PWM_FUNCTION
 * .............................................
 */


/* FILE INCLUDE */
//#include "adc.h"
#include "driver.h"
#include "main_isr.h"
/* DEFINE SOME PARAMETERS */
#define ADCAL_SAMPLE_TM     1000
/* DEFINE SOME VARIABLES */
int32 ADCal_SampleSum[14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};  /*电压电流累加值*/
int16 ADCal_SumNum = 0; /*累加计数*/

/* FUNCTION */

/*
 *Fuction:                      init_adc
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the ADC
 */
void InitAdcNew(void)
{
    EALLOW;
    PieVectTable.ADCINT1 = &main_isr; //中断映射 等级5
    EDIS;
    PieCtrlRegs.PIEIER1.bit.INTx1    = 1;       // Enable INT 1.1 in the PIE
    IER |= M_INT1;                              // Enable CPU Interrupt 1

    InitAdc();
    AdcOffsetSelfCal();

    EALLOW;
    AdcRegs.ADCSAMPLEMODE.bit.SIMULEN0 = 0;
    //第一个数据不用
    AdcRegs.ADCSOC0CTL.bit.CHSEL     = 0x0B;    //set SOC0   channel select to ADCINB3    //预留
    AdcRegs.ADCSOC1CTL.bit.CHSEL     = 0x08;    //set SOC1   channel select to ADCINB0    HVBUS
    AdcRegs.ADCSOC2CTL.bit.CHSEL     = 0x09;    //set SOC2   channel select to ADCINB1    HVPORT
    AdcRegs.ADCSOC3CTL.bit.CHSEL     = 0x0E;    //set SOC3   channel select to ADCINB6    LV1
    AdcRegs.ADCSOC4CTL.bit.CHSEL     = 0x0C;    //set SOC4   channel select to ADCINB4    LV2
    AdcRegs.ADCSOC5CTL.bit.CHSEL     = 0x0A;    //set SOC5   channel select to ADCINB2    LV3
    AdcRegs.ADCSOC6CTL.bit.CHSEL     = 0x06;    //set SOC6   channel select to ADCINA6    CURR1
    AdcRegs.ADCSOC7CTL.bit.CHSEL     = 0x04;    //set SOC7   channel select to ADCINA4    CURR2
    AdcRegs.ADCSOC8CTL.bit.CHSEL     = 0x02;    //set SOC8   channel select to ADCINA2    CURR3
    AdcRegs.ADCSOC9CTL.bit.CHSEL     = 0x01;    //set SOC9   channel select to ADCINA1    CURR4
    AdcRegs.ADCSOC10CTL.bit.CHSEL    = 0x07;    //set SOC10  channel select to ADCINA7    TMP1
    AdcRegs.ADCSOC11CTL.bit.CHSEL    = 0x03;    //set SOC11  channel select to ADCINA3    TMP2
    AdcRegs.ADCSOC12CTL.bit.CHSEL    = 0x00;    //set SOC12  channel select to ADCINA0    TMP3
    AdcRegs.ADCSOC13CTL.bit.CHSEL    = 0x0F;    //set SOC13  channel select to ADCINB7    校准


    AdcRegs.ADCSOC0CTL.bit.TRIGSEL   = 0x00;    //set SOC0 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC1CTL.bit.TRIGSEL   = 0x00;    //set SOC1 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC2CTL.bit.TRIGSEL   = 0x00;    //set SOC2 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC3CTL.bit.TRIGSEL   = 0x00;    //set SOC3 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC4CTL.bit.TRIGSEL   = 0x00;    //set SOC4 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC5CTL.bit.TRIGSEL   = 0x00;    //set SOC5 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC6CTL.bit.TRIGSEL   = 0x00;    //set SOC6 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC7CTL.bit.TRIGSEL   = 0x00;    //set SOC7 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC8CTL.bit.TRIGSEL   = 0x00;    //set SOC8 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC9CTL.bit.TRIGSEL   = 0x00;    //set SOC9 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC10CTL.bit.TRIGSEL  = 0x00;    //set SOC10 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC11CTL.bit.TRIGSEL  = 0x00;    //set SOC11 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC12CTL.bit.TRIGSEL  = 0x00;    //set SOC12 start trigger on Software, due to round-robin SOC0 convert
    AdcRegs.ADCSOC13CTL.bit.TRIGSEL  = 0x00;    //set SOC13 start trigger on Software, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC0CTL.bit.TRIGSEL   = 0x05;    //set SOC0  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC1CTL.bit.TRIGSEL   = 0x05;    //set SOC1  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC2CTL.bit.TRIGSEL   = 0x05;    //set SOC2  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC3CTL.bit.TRIGSEL   = 0x05;    //set SOC3  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC4CTL.bit.TRIGSEL   = 0x05;    //set SOC4  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC5CTL.bit.TRIGSEL   = 0x05;    //set SOC5  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC6CTL.bit.TRIGSEL   = 0x05;    //set SOC6  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC7CTL.bit.TRIGSEL   = 0x05;    //set SOC7  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC8CTL.bit.TRIGSEL   = 0x05;    //set SOC8  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC9CTL.bit.TRIGSEL   = 0x05;    //set SOC9  start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC10CTL.bit.TRIGSEL  = 0x05;    //set SOC10 start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC11CTL.bit.TRIGSEL  = 0x05;    //set SOC11 start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC12CTL.bit.TRIGSEL  = 0x05;    //set SOC12 start trigger on EPWM1A, due to round-robin SOC0 convert
//    AdcRegs.ADCSOC13CTL.bit.TRIGSEL  = 0x05;    //set SOC13 start trigger on EPWM1A, due to round-robin SOC0 convert

    AdcRegs.ADCSOC0CTL.bit.ACQPS     = 6;       //set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC1CTL.bit.ACQPS     = 6;       //set SOC1 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC2CTL.bit.ACQPS     = 6;       //set SOC2 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC3CTL.bit.ACQPS     = 6;       //set SOC3 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC4CTL.bit.ACQPS     = 6;       //set SOC4 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC5CTL.bit.ACQPS     = 6;       //set SOC5 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC6CTL.bit.ACQPS     = 6;       //set SOC6 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC7CTL.bit.ACQPS     = 6;       //set SOC7 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC8CTL.bit.ACQPS     = 6;       //set SOC8 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC9CTL.bit.ACQPS     = 6;       //set SOC9 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC10CTL.bit.ACQPS    = 6;       //set SOC10 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC11CTL.bit.ACQPS    = 6;       //set SOC11 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC12CTL.bit.ACQPS    = 6;       //set SOC12 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
    AdcRegs.ADCSOC13CTL.bit.ACQPS    = 6;       //set SOC13 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

    AdcRegs.ADCCTL1.bit.INTPULSEPOS  = 1;       //ADCINT1 trips after AdcResults latch
    AdcRegs.INTSEL1N2.bit.INT1E      = 1;       //Enabled ADCINT1
    AdcRegs.INTSEL1N2.bit.INT1CONT   = 0;       //Disable ADCINT1 Continuous mode
    AdcRegs.INTSEL1N2.bit.INT1SEL    = 0x0D;    //setup EOC13 to trigger ADCINT1 to fire
    AdcRegs.SOCPRICTL.bit.SOCPRIORITY = 0x0E;   //set SOC0-SOC13 high priority
    AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;       //清中断

    EDIS;
}

void ADCalibration(void)
{
    Uint16 k;
    for(k=0;k<13;k++)
    {
        ADCal_SampleSum[k] = 0;
    }
    ADCal_SumNum = 0;

    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);

    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 60, 40);
    EALLOW;
    PieVectTable.TINT0 = &ADCalInterrupt;
    EDIS;
    CpuTimer0Regs.TCR.all = 0x4000;
//    IER |= M_INT1;
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
    EINT;
    ERTM;
    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);
    DELAY_US(20000);
    DINT;
//    IER = 0x0000;
//    IFR = 0x0000;
    PieCtrlRegs.PIEIER1.bit.INTx7 = 0;

//    after alibration turn adc trigger to EPWM1A
    EALLOW;
    AdcRegs.ADCSOC0CTL.bit.TRIGSEL   = 0x05;    //set SOC0  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC1CTL.bit.TRIGSEL   = 0x05;    //set SOC1  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC2CTL.bit.TRIGSEL   = 0x05;    //set SOC2  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC3CTL.bit.TRIGSEL   = 0x05;    //set SOC3  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC4CTL.bit.TRIGSEL   = 0x05;    //set SOC4  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC5CTL.bit.TRIGSEL   = 0x05;    //set SOC5  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC6CTL.bit.TRIGSEL   = 0x05;    //set SOC6  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC7CTL.bit.TRIGSEL   = 0x05;    //set SOC7  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC8CTL.bit.TRIGSEL   = 0x05;    //set SOC8  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC9CTL.bit.TRIGSEL   = 0x05;    //set SOC9  start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC10CTL.bit.TRIGSEL  = 0x05;    //set SOC10 start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC11CTL.bit.TRIGSEL  = 0x05;    //set SOC11 start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC12CTL.bit.TRIGSEL  = 0x05;    //set SOC12 start trigger on EPWM1A, due to round-robin SOC0 convert
    AdcRegs.ADCSOC13CTL.bit.TRIGSEL  = 0x05;    //set SOC12 start trigger on EPWM1A, due to round-robin SOC0 convert

    AdcRegs.ADCCTL1.bit.INTPULSEPOS  = 1;       //ADCINT1 trips after AdcResults latch
    AdcRegs.INTSEL1N2.bit.INT1E      = 1;       //Enabled ADCINT1
    AdcRegs.INTSEL1N2.bit.INT1CONT   = 0;       //Disable ADCINT1 Continuous mode
    AdcRegs.INTSEL1N2.bit.INT1SEL    = 0x0D;    //setup EOC13 to trigger ADCINT1 to fire
    AdcRegs.SOCPRICTL.bit.SOCPRIORITY = 0x0E;   //set SOC0-SOC13 high priority
    AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;       //清中断
    EDIS;
}


__interrupt void  ADCalInterrupt(void)
{
    AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC1 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC2 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC3 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC4 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC5 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC6 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC7 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC8 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC9 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC10 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC11 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC12 = 1;
    AdcRegs.ADCSOCFRC1.bit.SOC13 = 1;

    DELAY_US(15);
    ADCal_SumNum ++;
    if(ADCal_SumNum == ADCAL_SAMPLE_TM)
    {
        offset.BACKUP = (int16)(ADCal_SampleSum[0]/ADCAL_SAMPLE_TM);
        offset.CURR1  = (int16)(ADCal_SampleSum[6]/ADCAL_SAMPLE_TM);
        offset.CURR2  = (int16)(ADCal_SampleSum[7]/ADCAL_SAMPLE_TM);
        offset.CURR3  = (int16)(ADCal_SampleSum[8]/ADCAL_SAMPLE_TM);
        offset.CURR4  = (int16)(ADCal_SampleSum[9]/ADCAL_SAMPLE_TM);
        offset.CORRECTION = (int16)(ADCal_SampleSum[13]/ADCAL_SAMPLE_TM);
    }

    ADCal_SampleSum[0] += AdcResult.ADCRESULT0;
    ADCal_SampleSum[6] += AdcResult.ADCRESULT6;
    ADCal_SampleSum[7] += AdcResult.ADCRESULT7;
    ADCal_SampleSum[8] += AdcResult.ADCRESULT8;
    ADCal_SampleSum[9] += AdcResult.ADCRESULT9;
    ADCal_SampleSum[13] += AdcResult.ADCRESULT13;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
