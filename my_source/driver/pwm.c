/*
 * .............................................
 * Author:                      Meng Yu;
 * Company:                     TOPSCOMM;
 * Data:                        2019_03_27;
 * Edition:                     1.0;
 * Summary:                     PWM_FUNCTION
 * .............................................
 */

/* FILE INCLUDE */
#include "driver.h"
#include "main_isr.h"
/* DEFINE SOME PARAMETERS */
#define PWM1_INT_ENABLE  1
#define PWM2_INT_ENABLE  1
#define PWM3_INT_ENABLE  1
#define PWM4_INT_ENABLE  1

//#define TEST
#define PWM1_TIMER_TBPRD   3000 //3000 周期
//#define EPWM1_MAX_CMPA     2190 //2190
#define EPWM1_MIN_CMPA     0 //占空比    1500/3000 = 50%
//#define EPWM1_MAX_CMPB     2190
#define EPWM1_MIN_CMPB     0
#define EPWM1_TIME_DB      120 //死区时间      *16.67ns

#define PWM2_TIMER_TBPRD   3000
//#define EPWM2_MAX_CMPA     2190
#define EPWM2_MIN_CMPA     0
//#define EPWM2_MAX_CMPB     2190
#define EPWM2_MIN_CMPB     0
#define EPWM2_TIME_DB      120

#define PWM6_TIMER_TBPRD   3000
//#define EPWM2_MAX_CMPA     2190
#define EPWM6_MIN_CMPA     0
//#define EPWM6_MAX_CMPB     2190
//#define EPWM6_MIN_CMPB     0
#define EPWM6_TIME_DB      0

/* DEFINE SOME VARIABLES */
EPWM_INFO epwm1_info;
EPWM_INFO epwm2_info;


/* FUNCTION */

/*
 *Fuction:                      init_pwm
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the PWM
 */
void InitPwm(void)
{
    InitEPwm1Gpio();
    InitEPwm2Gpio();
    InitEPwm6Gpio();
    InitTzGpio();

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    EDIS;

    InitEPwm1();
    InitEPwm2();
    InitEPwm6();
    //PWM触发AD采样
    EALLOW;
    EPwm1Regs.ETSEL.bit.SOCAEN      = 1;             //SOCA
    EPwm1Regs.ETSEL.bit.SOCASEL     = 3;
    EPwm1Regs.ETPS.bit.SOCAPRD      = 1;
    EDIS;

    DisablePwm(); //关闭PWM
    DisableFan();
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;
}
/*
 *Fuction:                      init_EPwm1
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the PWM
 */
void InitEPwm1(void)
{
    EPwm1Regs.TBPRD = PWM1_TIMER_TBPRD;  //Set period 2999 TBCLKs
    EPwm1Regs.TBPHS.half.TBPHS = 0x0000; //相位
    EPwm1Regs.TBCTR = 0x0000; //计数时基

    EPwm1Regs.CMPA.half.CMPA = EPWM1_MIN_CMPA;
//    EPwm1Regs.CMPB = EPWM1_MIN_CMPA;

    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; //升降
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; //TB_DISABLE; //Disable phase loading
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW; //影子
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;


    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW; //
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

    EPwm1Regs.AQCTLA.bit.CAU        = AQ_CLEAR;    // Set PWM1A on event A, up count
    EPwm1Regs.AQCTLA.bit.CAD        = AQ_SET;  // Clear PWM1A on event A, down count
//    EPwm1Regs.AQCTLB.bit.CAU        = AQ_SET;    // Set PWM1B on event A, up count
//    EPwm1Regs.AQCTLB.bit.CAD        = AQ_CLEAR;  // Clear PWM1B on event A, down count

    //dead band control
    EPwm1Regs.DBCTL.bit.IN_MODE     = DBA_ALL;
    EPwm1Regs.DBCTL.bit.OUT_MODE    = DB_FULL_ENABLE;
    EPwm1Regs.DBCTL.bit.POLSEL      = DB_ACTV_HIC;
    EPwm1Regs.DBRED                 = EPWM1_TIME_DB;
    EPwm1Regs.DBFED                 = EPWM1_TIME_DB;

    //    初始化后立即装载锁存到低电平
    EPwm1Regs.AQSFRC.all            = 0xC0;
//    EPwm1Regs.AQCSFRC.all           = 5;

    EPwm1Regs.ETSEL.bit.INTEN = 0; // 禁止PWM中断
    //reserved EmergeProtect Config
    EALLOW;

    EPwm1Regs.TZSEL.bit.OSHT2       = 1; //IO管脚
    //输出2
    EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMP1OUT; // DCAH = Comparator 1 output
    EPwm1Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI; //DCAEVT1 = DCAH high
    EPwm1Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1; // DCAEVT1 = DCAEVT1 (not filtered)
    EPwm1Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC; //
    EPwm1Regs.TZSEL.bit.DCAEVT1 = 1;
    //输出1
    EPwm1Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_COMP2OUT; // DCAL = Comparator 2 output
    EPwm1Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCBH_HI; //DCAEVT2 = DCBH high
    EPwm1Regs.DCACTL.bit.EVT2SRCSEL = DC_EVT1; // DCAEVT2 = DCAEVT2 (not filtered)
    EPwm1Regs.DCACTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC; //
    EPwm1Regs.TZSEL.bit.DCAEVT2 = 1;
    //输入总
//    EPwm1Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_COMP3OUT; //COPM3
//    EPwm1Regs.TZDCSEL.bit.DCBEVT1 = 2;
//    EPwm1Regs.DCBCTL.bit.EVT1SRCSEL = 0;
//    EPwm1Regs.DCBCTL.bit.EVT1FRCSYNCSEL = 0; //
//    EPwm1Regs.TZSEL.bit.DCBEVT1 = 1;

    EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm1Regs.TZCTL.bit.TZB = TZ_FORCE_LO;

    EDIS;

}
/*
 *Fuction:                      init_EPwm2
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the PWM2
 */
void InitEPwm2(void)
{
       EPwm2Regs.TBPRD = PWM2_TIMER_TBPRD;  //Set period 2999 TBCLKs
       EPwm2Regs.TBPHS.half.TBPHS = PWM2_TIMER_TBPRD;  //相位差半个周期，180度
       EPwm2Regs.TBCTR = 0x0000;

       EPwm2Regs.CMPA.half.CMPA = EPWM2_MIN_CMPA;
//       EPwm2Regs.CMPB = EPWM2_MIN_CMPA;

       EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
       EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;
       EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW; //影子
       EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
       EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

       EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;

       EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
       EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
       EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
       EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

       EPwm2Regs.AQCTLA.bit.CAU        = AQ_CLEAR;    // Set PWM2A on event A, up count
       EPwm2Regs.AQCTLA.bit.CAD        = AQ_SET;  // Clear PWM2A on event A, down count
//       EPwm2Regs.AQCTLB.bit.CAU        = AQ_SET;    // Set PWM2B on event A, up count
//       EPwm2Regs.AQCTLB.bit.CAD        = AQ_CLEAR;  // Clear PWM2B on event A, down count


       //dead band control
       EPwm2Regs.DBCTL.bit.IN_MODE     = DBA_ALL;
       EPwm2Regs.DBCTL.bit.OUT_MODE    = DB_FULL_ENABLE;
       EPwm2Regs.DBCTL.bit.POLSEL      = DB_ACTV_HIC;
       EPwm2Regs.DBRED                 = EPWM1_TIME_DB;
       EPwm2Regs.DBFED                 = EPWM1_TIME_DB;

       //    初始化后立即装载锁存到低电平
       EPwm2Regs.AQSFRC.all            = 0xC0;
//       EPwm2Regs.AQCSFRC.all           = 5;

       EPwm2Regs.ETSEL.bit.INTEN = 0; // 禁止PWM中断
       //reserved EmergeProtect Config
       EALLOW;

        EPwm2Regs.TZSEL.bit.OSHT2       = 1; //IO管脚
        //输出2
        EPwm2Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMP1OUT; // DCAH = Comparator 1 output
        EPwm2Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI; //DCAEVT1 = DCAH high
        EPwm2Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1; // DCAEVT1 = DCAEVT1 (not filtered)
        EPwm2Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC; //
        EPwm2Regs.TZSEL.bit.DCAEVT1 = 1;
        //输出1
        EPwm2Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_COMP2OUT; // DCAL = Comparator 2 output
        EPwm2Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCBH_HI; //DCAEVT2 = DCBH high
        EPwm2Regs.DCACTL.bit.EVT2SRCSEL = DC_EVT1; // DCAEVT2 = DCAEVT2 (not filtered)
        EPwm2Regs.DCACTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC; //
        EPwm2Regs.TZSEL.bit.DCAEVT2 = 1;
        //输入总
//        EPwm2Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_COMP3OUT; //COPM3
//        EPwm2Regs.TZDCSEL.bit.DCBEVT1 = 2;
//        EPwm2Regs.DCBCTL.bit.EVT1SRCSEL = 0;
//        EPwm2Regs.DCBCTL.bit.EVT1FRCSYNCSEL = 0; //同步
//        EPwm2Regs.TZSEL.bit.DCBEVT1 = 1;

        EPwm2Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
        EPwm2Regs.TZCTL.bit.TZB = TZ_FORCE_LO;

        EDIS;
}

void InitEPwm6(void)
{
       EPwm6Regs.TBPRD = PWM6_TIMER_TBPRD;  //Set period 2999 TBCLKs
       EPwm6Regs.TBPHS.half.TBPHS = 0;
       EPwm6Regs.TBCTR = 0x0000;

       EPwm6Regs.CMPA.half.CMPA = EPWM6_MIN_CMPA;
//       EPwm6Regs.CMPB = EPWM6_MIN_CMPA;

       EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
       EPwm6Regs.TBCTL.bit.PHSEN = TB_DISABLE;
       EPwm6Regs.TBCTL.bit.PRDLD = TB_SHADOW; //影子
       EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
       EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;

       EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;

       EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
       EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
       EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
       EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

       EPwm6Regs.AQCTLA.bit.CAU        = AQ_CLEAR;    // Set PWM2A on event A, up count
       EPwm6Regs.AQCTLA.bit.CAD        = AQ_SET;  // Clear PWM2A on event A, down count
//       EPwm6Regs.AQCTLB.bit.CAU        = AQ_CLEAR;    // Set PWM2B on event A, up count
//       EPwm6Regs.AQCTLB.bit.CAD        = AQ_SET;  // Clear PWM2B on event A, down count


       //dead band control
       EPwm6Regs.DBCTL.bit.IN_MODE     = DBA_ALL;
       EPwm6Regs.DBCTL.bit.OUT_MODE    = DBB_ENABLE;
       EPwm6Regs.DBCTL.bit.POLSEL      = DB_ACTV_HI;
       EPwm6Regs.DBRED                 = EPWM6_TIME_DB;
       EPwm6Regs.DBFED                 = EPWM6_TIME_DB;

       //    初始化后立即装载锁存到低电平
       EPwm6Regs.AQSFRC.all            = 0xC0;
//       EPwm2Regs.AQCSFRC.all           = 5;

       EPwm6Regs.ETSEL.bit.INTEN = 0; // 禁止PWM中断
       //reserved EmergeProtect Config


}

void DisablePwm(void)
{
//    EPwm1Regs.AQSFRC.all = 0xC0;
    EPwm1Regs.AQCSFRC.all = 5;
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

//    EPwm2Regs.AQSFRC.all = 0xC0;
    EPwm2Regs.AQCSFRC.all = 5;
    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

}
void DisableAPwm(void)
{
    EPwm1Regs.AQCSFRC.bit.CSFA = 1; //低
    EPwm1Regs.DBCTL.bit.OUT_MODE = DBB_ENABLE; //关A
    EPwm2Regs.AQCSFRC.bit.CSFA = 1; //低
    EPwm2Regs.DBCTL.bit.OUT_MODE = DBB_ENABLE; //关A

}
void DisableBPwm(void)
{
    EPwm1Regs.AQCSFRC.bit.CSFB = 1; //低
    EPwm1Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE; //关B
    EPwm2Regs.AQCSFRC.bit.CSFB = 1; //低
    EPwm2Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE; //关B

}
void EnablePwm(void)
{
    EPwm1Regs.AQCSFRC.all = 0;
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;

    EPwm2Regs.AQCSFRC.all = 0;
    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;

}
void DisableFan(void)
{
    EPwm6Regs.AQCSFRC.all = 5;
    EPwm6Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

}
void EnableFan(void)
{
    EPwm6Regs.AQCSFRC.all = 0;
    EPwm6Regs.DBCTL.bit.OUT_MODE = DBB_ENABLE;

}
