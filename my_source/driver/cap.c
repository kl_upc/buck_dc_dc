/*
 * .............................................
 * Author:
 * Company:                     TOPSCOMM;
 * Data:                        2019_04_18;
 * Edition:                     1.0;
 * Summary:                     CAP_FUNCTION
 * .............................................
 */


/* FILE INCLUDE */
//#include "cap.h"
#include "driver.h"
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */
Uint16 cap1_first;
Uint16 cap2_first;
Uint16 PULSELOW_1;
Uint16 PULSEHIGH_1;
Uint16 PULSELOW_2;
Uint16 PULSEHIGH_2;
/* FUNCTION */

/*
 *Fuction:                      init_cap
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          Initialize the ECAP
 */
void init_cap(void)
{
    InitECapGpio();
    //ecap use as apwm
    EALLOW;

    ECap1Regs.ECCTL2.bit.CAP_APWM = 1;
    ECap1Regs.CAP1 = 6000;
    ECap1Regs.CAP2 = 0;
    ECap1Regs.CTRPHS = 0x0; // make phase zero
    ECap1Regs.ECCTL2.bit.APWMPOL = 0; // Active high
    ECap1Regs.ECCTL2.bit.SYNCI_EN = 0; // Synch not used
    ECap1Regs.ECCTL2.bit.SYNCO_SEL = 2; // Synch not used
    ECap1Regs.ECCTL2.bit.TSCTRSTOP = 1; // Allow TSCTR to run
    EDIS;
//    ECap1Regs.ECCLR.all = 0xFF;
//    ECap1Regs.ECEINT.bit.CTR_EQ_CMP = 1;


    //HRCAP USE AS CAP
//    InitHRCapGpio();
//
//    EALLOW;
//    PieVectTable.HRCAP1_INT = &hrcap1_isr;
//    PieVectTable.HRCAP2_INT = &hrcap2_isr;
//    EDIS;
//
//    EALLOW;
//    HRCap1Regs.HCCTL.bit.HCCAPCLKSEL = 1;
//    HRCap1Regs.HCCTL.bit.RISEINTE = 1;
//    HRCap1Regs.HCCTL.bit.OVFINTE = 0;
//    HRCap1Regs.HCCTL.bit.FALLINTE = 0;
//
//    HRCap2Regs.HCCTL.bit.HCCAPCLKSEL = 1;
//    HRCap2Regs.HCCTL.bit.FALLINTE = 0;
//    HRCap2Regs.HCCTL.bit.RISEINTE = 1;
//    HRCap2Regs.HCCTL.bit.OVFINTE = 0;
//
//    HRCap1Regs.HCCTL.bit.SOFTRESET = 1;
//    HRCap2Regs.HCCTL.bit.SOFTRESET = 1;
//
//    EDIS;
//
//    PieCtrlRegs.PIEIER4.bit.INTx7 = 1;
//    PieCtrlRegs.PIEIER4.bit.INTx8 = 1;
//    IER|=M_INT4;
//    cap1_first = 0;
//    cap2_first = 0;
}
/*
 *Fuction:                      hrcap1_isr
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          isr for hrcap_1
 */
__interrupt void hrcap1_isr(void)
{
    EALLOW;
    if(HRCap1Regs.HCIFR.bit.RISEOVF == 1)
    {
        ESTOP0;                                 //ESTOP0 汇编仿真命令，相当与断点，正常程序运行相当于NOP;
    }
    if(cap1_first == 0)
    {
        cap1_first = 1;
    }
    else
    {
        HRCap1Regs.HCCTL.bit.RISEINTE = 0;
        PULSELOW_1 = HRCap1Regs.HCCAPCNTRISE0 + 1;
        PULSEHIGH_1 = HRCap1Regs.HCCAPCNTFALL1 + 1;
    }
    HRCap1Regs.HCICLR.all = 0x001F;
    HRCap1Regs.HCCTL.bit.RISEINTE = 1;
    HRCap1Regs.HCICLR.bit.INT = 1;
    PieCtrlRegs.PIEACK.bit.ACK4 = 1;
    EDIS;
}
/*
 *Fuction:                      hrcap2_isr
 *Parameter:                    void
 *Return:                       void
 *Summary:
 *          isr for hrcap_2
 */
__interrupt void hrcap2_isr(void)
{
    EALLOW;
    if(HRCap2Regs.HCIFR.bit.RISEOVF == 1)
    {
        ESTOP0;
    }
    if(cap2_first == 0)
    {
        cap2_first = 1;
    }
    else
    {
        HRCap2Regs.HCCTL.bit.RISEINTE = 0;
        PULSELOW_2 = HRCap2Regs.HCCAPCNTRISE0 + 1;
        PULSEHIGH_2 = HRCap2Regs.HCCAPCNTFALL1 + 1;
    }
    HRCap2Regs.HCICLR.all = 0x001F;
    HRCap2Regs.HCCTL.bit.RISEINTE = 1;
    HRCap2Regs.HCICLR.bit.INT = 1;
    PieCtrlRegs.PIEACK.bit.ACK4 = 1;
    EDIS;
}
