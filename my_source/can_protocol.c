/*
 * .............................................
 * Author:                      ;
 * Company:                     TOPSCOMM;
 * Data:                        2019_05_08;
 * Edition:                     1.0;
 * Summary:                     CanProtocol
 * .............................................
 */
/* FILE INCLUDE */
//#include "can_protocol.h"
#include "driver.h"

/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */
volatile struct MBOX *Mailbox15;
volatile struct MBOX *Mailbox16;
receive box15_receive = {0,0,0};
receive box16_receive = {0,0,0};
static  can_data receive_data = {0,0};
static  can_data send_data0 = {0,0};
static  can_data send_data1 = {0,0};
/* FUNCTION */

/*
 *Fuction:                      CAN_PROTOCOCOL
 *Parameter:                    Uint32 identifier, Uint32 lowdata,Uint32 highdata
 *Return:                       uint32 lens
 *Summary:
 *
 */
Uint32 CanProtocol(receive * receive_buffer)
{
    static  can_identifier  receive_identifier;
    //    static  can_identifier  send_identifier;

    receive_identifier.identifier_buffer = (*receive_buffer).id;
    receive_data.data_buffer[1] = (*receive_buffer).data1;
    receive_data.data_buffer[0] = (*receive_buffer).data2;
//    send_identifier.identifier_buffer = 0x00000000;
    send_data0.data_buffer[0] = 0x00000000;
    send_data0.data_buffer[1] = 0x00000000;
//    if((receive_identifier.identifier_struct.infor_type == 0xF8) && ((receive_identifier.identifier_struct.module_address == sys_run.sys_id.id_buffer) || (receive_identifier.identifier_struct.module_address == 0)))
	//广播指令
    if((receive_identifier.identifier_struct.infor_type == 0xF8) && (receive_identifier.identifier_struct.module_address == 0))
    {
        switch (receive_data.data_struct.CMD)
        {
            case 0x00:
            {
                sys_run.sys_set.value.set_current = receive_data.data_buffer[1];
                sys_run.sys_set.value.set_voltage = receive_data.data_buffer[0];
                send_data0.data_struct.CMD = 0x00;
                send_data0.data_struct.byte1 = 0xff;         //设置电压电流成功失败，需要讨论成功失败条件判断。
            }
            break;
            case 0x01:
            {
                send_data0.data_struct.CMD = 0x01;
                send_data0.data_struct.byte1 = 0x00;
                //电压
                send_data0.data_struct.byte2 = ((Uint32)(Cla_comm_data.i1sum * 10)) >> 8; //高位
                send_data0.data_struct.byte3 = Cla_comm_data.i1sum * 10 * 0x000000ff; //低位
                //电流
//                send_data0.data_struct.byte4 = ((Uint32)(cla_test6 * 10)) >> 8;
//                send_data0.data_struct.byte5 = cla_test6 * 10 * 0x000000ff;
//                send_data0.data_struct.byte2 = (sys_run.sys_value.value.DcCurrtO * 10) >> 8; //高位
//                send_data0.data_struct.byte3 = (sys_run.sys_value.value.DcCurrtO * 10) * 0x000000ff; //低位
//                send_data0.data_struct.byte4 = (sys_run.sys_value.value.OutVolt * 10) >> 8;
//                send_data0.data_struct.byte5 = (sys_run.sys_value.value.OutVolt * 10) * 0x000000ff;
//                send_data0.data_struct.byte6 = sys_run.sys_state.state_buffer >> 8;
//                send_data0.data_struct.byte7 = sys_run.sys_state.state_buffer * 0x00ff;
            }
            break;
            case 0x02:
            {
                if(receive_data.data_struct.byte7 == 0x55)
                {
                    sys_run.sys_set.value.set_on_off = 1;
                }
                if(receive_data.data_struct.byte7 == 0xaa)
                {
                    sys_run.sys_set.value.set_on_off = 0;
                }
                send_data0.data_struct.CMD = 0x02;
                send_data0.data_struct.byte1 = 0xff;
            }
            break;
            default:
            break;
        }
    }
    //点对点指令，需回复
    if((receive_identifier.identifier_struct.infor_type == 0xF8)&&(receive_identifier.identifier_struct.module_address == sys_run.sys_id.id_buffer))
    {
    //            ECanaMboxes.MBOX0.MSGID.all = 0x9207C080;

    }
    //模块间均流数据，无需回复
    if(receive_identifier.identifier_struct.infor_type == 0xF0)
    {
        switch(sys_run.sys_id.id_buffer)
        {
            case 1:
                if(receive_identifier.identifier_struct.module_address ==2)
                {
                    CanReDateTemp1.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp1.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp1.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp1.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i2sum = CanReDateTemp1.Float32Y;
                    run_parameter.s2    = receive_data.data_buffer[1];
                }
                if(receive_identifier.identifier_struct.module_address ==3)
                {
                    CanReDateTemp2.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp2.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp2.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp2.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i3sum = CanReDateTemp2.Float32Y;
                    run_parameter.s3    = receive_data.data_buffer[1];
                }
                break;
            case 2:
                if(receive_identifier.identifier_struct.module_address ==1)
                {
                    CanReDateTemp1.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp1.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp1.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp1.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i2sum = CanReDateTemp1.Float32Y;
                    run_parameter.s2    = receive_data.data_buffer[1];
                }
                if(receive_identifier.identifier_struct.module_address ==3)
                {
                    CanReDateTemp2.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp2.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp2.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp2.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i3sum = CanReDateTemp2.Float32Y;
                    run_parameter.s3    = receive_data.data_buffer[1];
                }
                break;
            case 3:
                if(receive_identifier.identifier_struct.module_address ==1)
                {
                    CanReDateTemp1.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp1.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp1.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp1.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i2sum = CanReDateTemp1.Float32Y;
                    run_parameter.s2    = receive_data.data_buffer[1];
                }
                if(receive_identifier.identifier_struct.module_address ==2)
                {
                    CanReDateTemp2.data_struct.byte1 = receive_data.data_struct.byte4;
                    CanReDateTemp2.data_struct.byte2 = receive_data.data_struct.byte5;
                    CanReDateTemp2.data_struct.byte3 = receive_data.data_struct.byte6;
                    CanReDateTemp2.data_struct.byte4 = receive_data.data_struct.byte7;
                    run_parameter.i3sum = CanReDateTemp2.Float32Y;
                    run_parameter.s3    = receive_data.data_buffer[1];
                }
                break;
            default: break;
        }

//        sys_run.sys_average_current[receive_identifier.identifier_struct.module_address - 1] = (Uint32)((receive_data.data_struct.byte2 << 8) + receive_data.data_struct.byte1);
    }
    return 0;
}
/*
 *Fuction:                      CAN_TASK
 *Parameter:                    VOID
 *Return:                       VOID
 *Summary:
 *
 */
void CanTask(void)
{
    if(ECanaRegs.CANRMP.bit.RMP15 == 1)
    {

        ECanaRegs.CANRMP.bit.RMP15 = 1;
        Mailbox15 = &ECanaMboxes.MBOX0 + 15;
        box15_receive.data1 = Mailbox15->MDL.all;
        box15_receive.data2 = Mailbox15->MDH.all;
        box15_receive.id = Mailbox15->MSGID.all;

        CanProtocol(&box15_receive);
    }
    if(ECanaRegs.CANRMP.bit.RMP16 == 1)
    {
        ECanaRegs.CANRMP.bit.RMP16 = 1;
        Mailbox16 = &ECanaMboxes.MBOX0 + 16;
        box16_receive.data1 = Mailbox16->MDL.all;
        box16_receive.data2 = Mailbox16->MDH.all;
        box16_receive.id = Mailbox16->MSGID.all;
        CanProtocol(&box16_receive);
    }
}

/*
 *Fuction:                      CAN_REPORT_TASK
 *Parameter:                    VOID
 *Return:                       VOID
 *Summary:
 *
 */
void CanReportTask(void)
{
//    if((1 == sys_run.sys_can_state.host_need_report_onff) && (1))
//    {
//        canbox0_send();
//        sys_run.sys_can_state.host_need_report_onff = 0;
//    }
//    if((1 == sys_run.sys_can_state.host_need_report_mode) && (1))
//    {
//        canbox0_send();
//        sys_run.sys_can_state.host_need_report_mode = 0;
//    }
//    if(1 == sys_run.sys_can_state.host_need_report_data)
//    {
//        canbox0_send();
//        sys_run.sys_can_state.host_need_report_data = 0;
//    }
    if(1 == sys_run.sys_can_state.averge_current_report)
    {
        //发送电流和状态
//        send_data1.data_buffer[0] = Cla_comm_data_com.i1sum * 10 + 10000;
        CanSendDateTemp1.Float32Y   = Cla_comm_data.i1sum;
        send_data1.data_buffer[1] = Cla_comm_data.s1;
        if(Cla_comm_data.s1)
        {
            send_data1.data_struct.byte4 = CanSendDateTemp1.data_struct.byte1;
            send_data1.data_struct.byte5 = CanSendDateTemp1.data_struct.byte2;
            send_data1.data_struct.byte6 = CanSendDateTemp1.data_struct.byte3;
            send_data1.data_struct.byte7 = CanSendDateTemp1.data_struct.byte4;
        }
        else
            send_data1.data_buffer[0] = 0;
        canbox1_send();
        //发送其它数据
        send_data0.data_buffer[0] = cla_test1 ;
        send_data0.data_buffer[1] = cla_test2 ;
//        canbox0_send();
        sys_run.sys_can_state.averge_current_report = 0;
    }
    return;
}
/*
 *Fuction:                      CANBOX0_SEND
 *Parameter:                    VOID
 *Return:                       VOID
 *Summary:
 *
 */
void canbox0_send(void)
{
//    ECanaMboxes.MBOX0.MSGCTRL.bit.DLC = 8;
    ECanaMboxes.MBOX0.MDL.all = send_data0.data_buffer[1];
    ECanaMboxes.MBOX0.MDH.all = send_data0.data_buffer[0];
    ECanaShadow.CANTRS.all = 0;
    ECanaShadow.CANTRS.bit.TRS0 = 1; //发送请求置位
    ECanaRegs.CANTRS.all =  ECanaShadow.CANTRS.all;
    do
    {
        ECanaShadow.CANTA.all = ECanaRegs.CANTA.all;
    }while(ECanaShadow.CANTA.bit.TA0 == 0);
    ECanaShadow.CANTA.all = 0;
    ECanaShadow.CANTA.bit.TA0 = 1;
    ECanaRegs.CANTA.all = ECanaShadow.CANTA.all;
    return;
}
/*
 *Fuction:                      CANBOX1_SEND
 *Parameter:                    VOID
 *Return:                       VOID
 *Summary:
 *
 */
void canbox1_send(void)
{
//    ECanaMboxes.MBOX1.MSGCTRL.bit.DLC = 8;
    ECanaMboxes.MBOX1.MDL.all = send_data1.data_buffer[1];
    ECanaMboxes.MBOX1.MDH.all = send_data1.data_buffer[0];
    ECanaShadow.CANTRS.all = 0;
    ECanaShadow.CANTRS.bit.TRS1 = 1;
    ECanaRegs.CANTRS.all =  ECanaShadow.CANTRS.all;
    do
    {
        ECanaShadow.CANTA.all = ECanaRegs.CANTA.all;
    }while(ECanaShadow.CANTA.bit.TA1 == 0);
    ECanaShadow.CANTA.all = 0;
    ECanaShadow.CANTA.bit.TA1 = 1;
    ECanaRegs.CANTA.all = ECanaShadow.CANTA.all;
    return;
}
