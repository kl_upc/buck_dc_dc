/*
 * .............................................
 * Author:
 * Company:                     TOPSCOMM;
 * Data:                        2019_05_21;
 * Edition:                     1.0;
 * Summary:                     sys_run_data
 * .............................................
 */
/* FILE INCLUDE */
#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "driver.h"
#include <string.h>
#include <stdint.h>
/* DEFINE SOME PARAMETERS */

/* DEFINE SOME VARIABLES */
sys_run_parameter sys_run = {
                             0,
                             0,
                             {0},
                             {0,0,0},
                             {0,0,0,0,0,0},
                             {0,0,0,0,0,0},
                             {0},
                             {0,0,0,0},
};

Uint16 uart_r = 0; //串口接收字
// 发送故障码
Uint16 send_fault = 0;
Uint16 Fault_shift = 0 ;
//通信传输
CharToFloat CanSendDateTemp1 = {0};
CharToFloat CanSendDateTemp2  = {0};
CharToFloat CanSendDateTemp10  = {0};
CharToFloat CanSendDateTemp20  = {0};
CharToFloat CanReDateTemp1  = {0};
CharToFloat CanReDateTemp2  = {0};
CharToFloat CanReDateTemp10  = {0};
CharToFloat CanReDateTemp20  = {0};
/* FUNCTION */

void DspInit(void)
{
    InitSysCtrl();
    memcpy((uint16_t *)&RamfuncsRunStart,(uint16_t *)&RamfuncsLoadStart, (unsigned long)&RamfuncsLoadSize);
    InitFlash();
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    InitGpio();
    InitCla();
    InitAdcNew();
    ADCalibration();
    InitPwm();
    InitTime(); //20ms中断交互一次can数据
    InitSciNew();
    InitComparator();
    InitCan();
    GetSysId();
    sys_id = sys_run.sys_id.id_buffer;
    InitCanbox();
    //    init_cap();
    //    init_iic();
    EALLOW;
    GpioCtrlRegs.GPADIR.bit.GPIO7 = 1; //主开关
    DisablePowerOut();
    GpioCtrlRegs.GPADIR.bit.GPIO6 = 1; //充电
    DisableChargOut();
    GpioCtrlRegs.GPADIR.bit.GPIO10 = 1; //PWM驱动使能信号
    DisablePwm();
    GpioCtrlRegs.GPADIR.bit.GPIO4 = 1; //故障锁存清除信号
    DisableFaultClrOut();
    GpioCtrlRegs.GPADIR.bit.GPIO5 = 1; //控制放电电路
    DisableDischarge() ; //不放电
    GpioCtrlRegs.GPADIR.bit.GPIO18 = 1; //485使能
    Enable485Re(); //使能接收
    GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1; //读取故障锁存脉冲
    DisableSHIFTCLK();
    GpioCtrlRegs.GPADIR.bit.GPIO22 = 1; //故障锁存一次
    DisableLATCHEN();
    DINT;
}
/***************************************************************/
void GetSysId(void)
{
    sys_run.sys_id.id.address1 = ~GpioDataRegs.GPADAT.bit.GPIO9; //黄灯
    sys_run.sys_id.id.address2 = ~GpioDataRegs.GPADAT.bit.GPIO21; //绿灯
    sys_run.sys_id.id.address3 = 0;//GpioDataRegs.GPADAT.bit.GPIO20; //红灯
    sys_run.sys_id.id.address4 = 0;
    sys_run.sys_id.id.address5 = 0;
    sys_run.sys_id.id.address6 = 0;
    sys_run.sys_id.id.address7 = 0;
    sys_run.sys_id.id.reserve  = 0;
}
/***************************************************************/
void SciComm(void)
{
    switch (uart_r)
    {
        case 120: //增加占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA + 200;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA + 200;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA / 10);
               uart_r = 0;
            break;
        case 121: //减去占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA - 200;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA - 200;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA / 10);
               uart_r = 0;
            break;
        case 110: //增加占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA + 1;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA + 1;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA );
               uart_r = 0;
            break;
        case 111: //减去占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA - 1;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA - 1;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA );
               uart_r = 0;
            break;
        case 105: //增加占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA + 50;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA + 50;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA / 10);
               uart_r = 0;
            break;
        case 106: //减去占空比
               EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA - 50;
               EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA - 50;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(EPwm1Regs.CMPA.half.CMPA / 10);
               uart_r = 0;
            break;
        case 102: //增加占空比
                EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA + 20;
                EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA + 20;
                EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
                EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
                send485(EPwm1Regs.CMPA.half.CMPA / 10);
                uart_r = 0;
            break;
        case 103: //减去占空比
                EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA - 20;
                EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA - 20;
                EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
                EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
                send485(EPwm1Regs.CMPA.half.CMPA / 10);
                uart_r = 0;
            break;
        case 100: //增加占空比
                EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA + 10;
                EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA + 10;
                EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
                EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
                send485(EPwm1Regs.CMPA.half.CMPA / 10);
                uart_r = 0;
            break;
        case 101: //减去占空比
                EPwm1Regs.CMPA.half.CMPA = EPwm1Regs.CMPA.half.CMPA - 10;
                EPwm2Regs.CMPA.half.CMPA = EPwm2Regs.CMPA.half.CMPA - 10;
                EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
                EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
                send485(EPwm1Regs.CMPA.half.CMPA / 10);
                uart_r = 0;
            break;
        case 69: //
            run_parameter.IOutMax_test = run_parameter.IOutMax_test - 5;
            run_parameter.IOutMin_test = run_parameter.IOutMin_test + 5;
            send485(uart_r);
            uart_r = 0;
            break;
        case 68: //
            run_parameter.IOutMax_test = run_parameter.IOutMax_test + 5;
            run_parameter.IOutMin_test = run_parameter.IOutMin_test - 5;
            send485(uart_r);
            uart_r = 0;
            break;
        case 67: //关闭上管PWMB
                DisableBPwm();
                send485(uart_r);
                uart_r = 0;
                break;
        case 66: //关闭下管PWMA
            DisableAPwm();
            send485(uart_r);
            uart_r = 0;
            break;
        case 65: //切出电压环
            run_parameter.state = 10;
            send485(uart_r);
            uart_r = 0;
            break;
        case 64: //切入电压环
            run_parameter.state = 2;
            send485(uart_r);
            uart_r = 0;
            break;
        case 63:
            run_parameter.set_u = run_parameter.set_u + 50;
            send485(uart_r);
            uart_r = 0;
            break;
        case 62:
            run_parameter.set_u = 700;
            send485(uart_r);
            uart_r = 0;
            break;
        case 61:
            run_parameter.set_u = 650;
            send485(uart_r);
            uart_r = 0;
            break;
        case 60:
            run_parameter.set_u = 600;
            send485(uart_r);
            uart_r = 0;
            break;
        case 59:
            run_parameter.set_u = 550;
            send485(uart_r);
            uart_r = 0;
            break;
        case 58:
            run_parameter.set_u = 500;
            send485(uart_r);
            uart_r = 0;
            break;
        case 57:
            run_parameter.set_u = 450;
            send485(uart_r);
            uart_r = 0;
            break;
        case 56:
            run_parameter.set_u = 400;
            send485(uart_r);
            uart_r = 0;
            break;
        case 55:
            run_parameter.set_u = 300;
            send485(uart_r);
            uart_r = 0;
            break;
        case 54:
            run_parameter.set_u = 250;
            send485(uart_r);
            uart_r = 0;
            break;
        case 53:
            run_parameter.set_u = 200;
            send485(uart_r);
            uart_r = 0;
            break;
        case 52:
            run_parameter.set_u = 100;
            send485(uart_r);
            uart_r = 0;
            break;
        case 51:
                run_parameter.set_u = 50;
                send485(uart_r);
                uart_r = 0;
                break;
        case 50:
                run_parameter.set_u = 30;
                send485(uart_r);
                uart_r = 0;
                break;
        case 49:
               EPwm1Regs.CMPA.half.CMPA = 1;
               EPwm2Regs.CMPA.half.CMPA = 1;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(uart_r);
               uart_r = 0;
               break;
        case 48:
               EPwm1Regs.CMPA.half.CMPA = 500;
               EPwm2Regs.CMPA.half.CMPA = 500;
               EPwm1Regs.CMPB = EPwm1Regs.CMPA.half.CMPA;
               EPwm2Regs.CMPB = EPwm2Regs.CMPA.half.CMPA;
               send485(uart_r);
               uart_r = 0;
               break;
        case 42: //读取故障
                send485(uart_r);
                send485(send_fault);
				send_fault = 0;
                uart_r = 0;
                break;
        case 41: //恒占空比开风扇
                EnableFan();
                run_parameter.Fan_state = 0;
                send485(uart_r);
                uart_r = 0;
                break;
        case 40:
                EPwm6Regs.CMPA.half.CMPA = 0;
                send485(EPwm6Regs.CMPA.half.CMPA / 10);
                send485(uart_r);
                uart_r = 0;
                break;
        case 39:
                EPwm6Regs.CMPA.half.CMPA = EPwm6Regs.CMPA.half.CMPA - 50;
                send485(EPwm6Regs.CMPA.half.CMPA / 10);
                send485(uart_r);
                uart_r = 0;
                break;
        case 38:
                EPwm6Regs.CMPA.half.CMPA = EPwm6Regs.CMPA.half.CMPA + 50;
                send485(EPwm6Regs.CMPA.half.CMPA / 10);
                send485(uart_r);
                uart_r = 0;
                break;
        case 37: //关风扇
                DisableFan();
                run_parameter.Fan_state = 0;
                send485(uart_r);
                uart_r = 0;
                break;
        case 36: //开风扇
                EnableFan();
                run_parameter.Fan_state = 1;
                send485(uart_r);
                uart_r = 0;
                break;

        case 35:
                run_parameter.Upi_ki_test = run_parameter.Upi_ki_test - 0.0005;
                send485(uart_r);
                uart_r = 0;
                break;
        case 34:
                run_parameter.Upi_ki_test = run_parameter.Upi_ki_test + 0.0005;
                send485(uart_r);
                uart_r = 0;
                break;
        case 33:
                run_parameter.Upi_ki_test = run_parameter.Upi_ki_test - 0.0001;
                send485(uart_r);
                uart_r = 0;
                break;
        case 32:
                run_parameter.Upi_ki_test = run_parameter.Upi_ki_test + 0.0001;
                send485(uart_r);
                uart_r = 0;
                break;
        case 29:
                run_parameter.Upi_kp_test = run_parameter.Upi_kp_test - 0.03;
                send485(uart_r);
                uart_r = 0;
                break;
        case 28:
                run_parameter.Upi_kp_test = run_parameter.Upi_kp_test + 0.03;
                send485(uart_r);
                uart_r = 0;
                break;
        case 27:
                run_parameter.Upi_kp_test = run_parameter.Upi_kp_test - 0.01;
                send485(uart_r);
                uart_r = 0;
                break;
        case 26:
                run_parameter.Upi_kp_test = run_parameter.Upi_kp_test + 0.01;
                send485(uart_r);
                uart_r = 0;
                break;
        case 25:
                run_parameter.pi_ki_test = run_parameter.pi_ki_test - 0.0005;
                send485(uart_r);
                uart_r = 0;
                break;
        case 24:
                run_parameter.pi_ki_test = run_parameter.pi_ki_test + 0.0005;
                send485(uart_r);
                uart_r = 0;
                break;
        case 23:
                run_parameter.pi_ki_test = run_parameter.pi_ki_test - 0.0001;
                send485(uart_r);
                uart_r = 0;
                break;
        case 22:
                run_parameter.pi_ki_test = run_parameter.pi_ki_test + 0.0001;
                send485(uart_r);
                uart_r = 0;
                break;
        case 21:
                run_parameter.pi_kp_test = run_parameter.pi_kp_test - 0.3;
                send485(uart_r);
                uart_r = 0;
                break;
        case 20:
                run_parameter.pi_kp_test = run_parameter.pi_kp_test + 0.3;
                send485(uart_r);
                uart_r = 0;
                break;
        case 19:
                run_parameter.pi_kp_test = run_parameter.pi_kp_test - 0.1;
                send485(uart_r);
                uart_r = 0;
                break;
        case 18:
                run_parameter.pi_kp_test = run_parameter.pi_kp_test + 0.1;
                send485(uart_r);
                uart_r = 0;
                break;

        case 16:
                run_parameter.set_i = run_parameter.set_i - 1;
                send485( uart_r);
                uart_r = 0;
                break;
        case 15:
                run_parameter.set_i = run_parameter.set_i + 1;
                send485( uart_r);
                uart_r = 0;
                break;
        case 14:
                run_parameter.set_i = 10;
                send485(uart_r);
                uart_r = 0;
                break;
        case 13:
                run_parameter.set_i = 1;
                send485(uart_r);
                uart_r = 0;
                break;
        case 12: //切出电流环
                run_parameter.state = 10;
                send485(uart_r);
                uart_r = 0;
                break;
        case 11: //切入电流环
            run_parameter.state = 1;
            send485(uart_r);
            uart_r = 0;
            break;
        case 10: //读取故障字
            send485(uart_r);

            for(Fault_shift = 0; Fault_shift<16; Fault_shift++)
            {
                switch(Fault_shift)
                {
                    case 0:
                            if(GpioDataRegs.GPADAT.bit.GPIO8 == 0) //Fault4
                            {
                                send_fault = send_fault | 1;
                            }
                            break;
                    case 1:
                            if(GpioDataRegs.GPADAT.bit.GPIO8 == 0) //Fault3
                            {
                                send_fault = send_fault | 2;
                            }
                            break;
                    case 2:
                            if(GpioDataRegs.GPADAT.bit.GPIO8 == 0) //Fault2
                            {
                                send_fault = send_fault | 4;
                            }
                            break;
                    case 3:
                            if(GpioDataRegs.GPADAT.bit.GPIO8 == 0) //Fault1
                            {
                                send_fault = send_fault | 8;
                            }
                            break;
                }
                //移位
                EnableSHIFTCLK();
                DELAY_US(300);
                DisableSHIFTCLK();
            }
            send485(send_fault); //返回故障位
            send_fault = 0;
            uart_r = 0;
            break;
       case 9:
            DisableDischarge(); //不放电
            send485(uart_r);
            uart_r = 0;
            break;
       case 8:
            EnableDischarge() ; //放电
            send485(uart_r);
            uart_r = 0;
            break;
        case 7: //清除故障
//            // 关PWM
            DisablePwm();
            run_parameter.start_state = 0;
            send485(uart_r);
            //清除锁存故障
            EnableFaultClrOut();
            DELAY_US(1000);
            //使能故障锁存
            DisableFaultClrOut();
            // 清除错误区标志
            EALLOW;
            EPwm1Regs.TZCLR.all = 0xFF;
            EPwm2Regs.TZCLR.all = 0xFF;
            EDIS;
            uart_r = 0;
            break;
        case 6: //pwm关机
            run_parameter.start_state = 0;
            DisablePwm();
            Cla_comm_data.s1 = 0;
            EnableDischarge() ; //放电
            send485(uart_r);
            uart_r = 0;
            break;
        case 5: //pwm开机
//            if(GpioDataRegs.GPADAT.bit.GPIO7 == 1) //合主开关后才能开机
//            {
            DisableDischarge(); //不放电
            run_parameter.start_state = 1;
            EnablePwm();
            Cla_comm_data.s1 = 1;
//            }
            send485(uart_r);
            uart_r = 0;
            break;
        case 4: //充电关闭
            DisableChargOut();
            send485(uart_r);
            uart_r = 0;
            break;
        case 3: //充电电阻充电
            EnableChargOut();
            send485(uart_r);
            uart_r = 0;
            break;
        case 2: //主开关关
            DisablePowerOut();
            send485(uart_r);
            uart_r = 0;
            break;
        case 1: //主开关开，充电关
//            if(result.LV3 > (result.HVBUS - 5)) //电压充到母线电压的90%以上则充电结束
//            {
                EnablePowerOut();
//            }
            send485(uart_r);
            uart_r = 0;
            break;
        default: break;
    }

}
/***************************************************************/
void send485(Uint16 senddata)
{
//    Enable485Send(); //使能发送
//    DELAY_US(300);
    if(SciaRegs.SCICTL2.bit.TXRDY == 1)
    {
      SciaRegs.SCITXBUF = senddata;
    }
//    while (SciaRegs.SCICTL2.bit.TXRDY == 0)
//    {
//    }
//    DELAY_US(200);
//    DELAY_US(5000);  /*低波特率时等待485发送完成*/
//    Enable485Re(); //使能接收

}
