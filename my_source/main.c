/*
 * .............................................
 * Author:                      ;
 * Company:                     TOPSCOMM;
 * Data:                        2019_03_27;
 * Edition:                     1.0;
 * Summary:                     BUCK_DC_DC
 *          CAN
 *          PWM
 *          ADC
 *          IIC
 *          CLA
 *          CAP
 *          COM
 * .............................................
 */

#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "driver.h"
#include <string.h>
#include <stdint.h>

int main(void)
{
    DspInit();

    EINT;
    ERTM;
    while(1)
    {

        SysTask();
        CanTask();
        CanReportTask();
//        SciComm(); // 485����
    }
}
/*****************************************************/



