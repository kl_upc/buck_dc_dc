#ifndef IIC_H_
#define IIC_H_
#include "DSP28x_Project.h"

void init_iic(void);
void eeprom_iic_write_byte(Uint16 addr,Uint16 data);
Uint16 eeprom_iic_write_page(Uint16 addr,Uint16 *buf,Uint16 len);
void eeprom_iic_write_section(Uint16 addr, Uint16 * buf, Uint16 len);
Uint16 eeprom_iic_read_byte(Uint16 addr);
Uint16 eeprom_iic_read_page(Uint16 addr, Uint16 * buf, Uint16 len);
void eeprom_iic_read_section(Uint16 addr, Uint16 * buf, Uint16 len);
#endif
