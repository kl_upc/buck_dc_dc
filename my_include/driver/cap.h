#ifndef CAP_H_
#define CAP_H_
#include "DSP28x_Project.h"
void init_cap(void);
__interrupt void hrcap1_isr(void);
__interrupt void hrcap2_isr(void);
#endif
