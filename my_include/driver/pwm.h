#ifndef PWM_H_
#define PWM_H_

#include "DSP28x_Project.h"
typedef struct
{
   volatile struct EPWM_REGS *EPwmRegHandle;
   Uint16 EPwmMaxCMPA;
   Uint16 EPwmMinCMPA;
   Uint16 EPwmMaxCMPB;
   Uint16 EPwmMinCMPB;
}EPWM_INFO;


void InitPwm(void);
void InitEPwm1(void);
void InitEPwm2(void);
void InitEPwm6(void);
void DisablePwm(void);
void DisableAPwm(void);
void DisableBPwm(void);
void EnablePwm(void);
void DisableFan(void);
void EnableFan(void);
#endif
