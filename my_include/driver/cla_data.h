#ifndef CLA_DATA_H_
#define CLA_DATA_H_
#include "DSP28x_Project.h"
typedef struct
{
    float32  Ref;           /*Input: Reference input*/
    float32  Fdb;           /*Input: Feedback input*/
    float32  Err;           /*Variable: Error*/
    float32  Kp;            /*Parameter: Proportional gain*/
    float32  Ki;            /*Parameter: Integral gain*/
    float32  Up;            /*Variable: Proportional output*/
    float32  Ui;            /*Variable: Integral output*/
    float32  OutPreSat;    /*Variable: Pre-saturated output*/
    float32  OutMax;        /*Parameter: Maximum output*/
    float32  OutMin;        /*Parameter: Minimum output*/
//    float32  UiMax;
//    float32  UiMin;
    float32  Out;           /*Output: PID output*/
//    float32  SatErr;        /*Variable: Saturated difference*/
//    float32  Kc;            /*Parameter: Integral correction gain*/
}PI;

//AD消除偏差
typedef struct
{
//    int16 HVBUS;
//    int16 HVPORT;
//    int16 LV1;
//    int16 LV2;
//    int16 LV3;
    int16 CURR1;
    int16 CURR2;
    int16 CURR3;
    int16 CURR4;
//    int16 TMP1;
//    int16 TMP2;
//    int16 TMP3;
    int16 BACKUP;
    int16 CORRECTION ;
}ad_offset;

typedef struct
{
    float32 HVBUS;
    float32 HVPORT;
    float32 LV1;
    float32 LV2;
    float32 LV3;
    float32 CURR1;
    float32 CURR2;
    float32 CURR3;
    float32 CURR4;
    float32 TMP1;
    float32 TMP2;
    float32 TMP3;
    float32 BACKUP;
}ad_result;
typedef struct
{
    float   UdcRef; //电压值
    float   IdcRef; //电流值
    float   Dm;     //占空比
    float   duty;   //占空比对应的比较值
//    float   BusCount;
}control;

typedef struct
{
    float   TempRef; //温度
    float   Temp; //温度采样值
    float   Dm;     //占空比
    float   duty;   //占空比对应的比较值
}control_fan;

typedef struct
{
    Uint16   start_state;
    Uint16   Fan_state;
    Uint16   state;
    float   set_i;
    float   set_u;
    float   set_fan_temp;
    float   fault_flag;
    float   pi_kp_test;
    float   pi_ki_test;
    float   Upi_kp_test;
    float   Upi_ki_test;
    float   IOutMax_test;
    float   IOutMin_test;
    Uint16   s2;
    Uint16   s3;
    float   i2sum;
    float   i3sum;
}cla_run;

typedef struct
{
    Uint16   s1;
    float i1sum;
}cla_comm_data;

extern  PI IPICtl1;
extern  PI IPICtl2;
extern  PI IPICtlcom;
extern  PI UPICtl;
extern  PI FAN;

extern  ad_offset offset;
extern  ad_result result;

extern  control ctrl1;
extern  control ctrl2;
extern  cla_run  run_parameter;
extern  control_fan ctrl_fan;
extern  cla_comm_data Cla_comm_data;
extern  Uint16   ModChange;
extern  float   UPICtl_temp;
extern  Uint16   FanState;
extern  Uint16  I_delaytime;
extern  Uint32  sys_id;
//测试
extern  Uint32 cla_test1;
extern  Uint32 cla_test2;
extern  float cla_test3;
extern  float cla_test4;


#endif


