#ifndef ADC_H_
#define ADC_H_

#include "DSP28x_Project.h"

void InitAdcNew(void);
void ADCalibration(void);
__interrupt void  ADCalInterrupt(void);
#endif
