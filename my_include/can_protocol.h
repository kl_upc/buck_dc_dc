#ifndef CAN_PROTOCOL_H_
#define CAN_PROTOCOL_H_
#include "DSP28x_Project.h"

typedef struct
{
    Uint32  id;
    Uint32  data1;
    Uint32  data2;
}receive;


typedef union
{
    Uint32 identifier_buffer;
    struct
    {
        Uint32  module_address      :7;
        Uint32  module_type         :4;
        Uint32  infor_type          :11;
        Uint32  frame_type          :3;
        Uint32  protocol_type       :4;
        Uint32  reserve             :3;
    }identifier_struct;
}can_identifier;

typedef union
{
    Uint32 data_buffer[2];
    struct
    {
        Uint32  byte7           :8;
        Uint32  byte6           :8;
        Uint32  byte5           :8;
        Uint32  byte4           :8;
        Uint32  byte3           :8;
        Uint32  byte2           :8;
        Uint32  byte1           :8;
        Uint32  CMD             :8;
    }data_struct;
}can_data;

Uint32 CanProtocol(receive * receive_buffer);
void CanTask(void);
void CanReportTask(void);
void canbox0_send(void);
void canbox1_send(void);
#endif
