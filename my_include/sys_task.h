#ifndef SYS_TASK_H_
#define SYS_TASK_H_
#include "DSP28x_Project.h"

enum state
{
    SYS_INIT = 0,
    SYS_CHECK,
    SYS_WAIT_OPEN,
    SYS_OPEN,
    SYS_SELECT_MODE,
    SYS_CURRENT_MODE,
    SYS_VOLTAGE_MODE,
    SYS_CLOSE,
    SYS_ERROR
};
void SysTask(void);
int sys_check(void);
void sys_init(void);

#endif
