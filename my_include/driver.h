#ifndef DRIVER_H_
#define DRIVER_H_
#include "adc.h"
#include "can.h"
#include "cap.h"
#include "cla_data.h"
#include "cla_task.h"
#include "cla.h"
#include "comparator.h"
#include "iic.h"
#include "pwm.h"
#include "sci.h"
#include "time.h"
#include "can_protocol.h"
#include "main_isr.h"
#include "sys_data.h"
#include "sys_task.h"

#define EnablePowerOut()      GpioDataRegs.GPASET.bit.GPIO7 = 1    /*主开关使能，停止充电*/
#define DisablePowerOut()     GpioDataRegs.GPACLEAR.bit.GPIO7 = 1

#define EnableChargOut()      GpioDataRegs.GPASET.bit.GPIO6 = 1    /*充电使能*/
#define DisableChargOut()     GpioDataRegs.GPACLEAR.bit.GPIO6 = 1

#define EnableSHCLKEN()      GpioDataRegs.GPACLEAR.bit.GPIO10 = 1    /*锁存电路时钟使能*/
#define DisableSHCLKEN()     GpioDataRegs.GPASET.bit.GPIO10 = 1

#define Enable485Send()     GpioDataRegs.GPASET.bit.GPIO18 = 1  //485通讯方向，发送
#define Enable485Re()        GpioDataRegs.GPACLEAR.bit.GPIO18 = 1 //485接收

#define EnableFaultClrOut()      GpioDataRegs.GPACLEAR.bit.GPIO4 = 1    /*清除故障锁存*/
#define DisableFaultClrOut()     GpioDataRegs.GPASET.bit.GPIO4 = 1

#define EnableDischarge()      GpioDataRegs.GPACLEAR.bit.GPIO5 = 1    /*放电*/
#define DisableDischarge()     GpioDataRegs.GPASET.bit.GPIO5 = 1

#define EnableSHIFTCLK()      GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1    /*读取一次故障*/
#define DisableSHIFTCLK()     GpioDataRegs.GPBSET.bit.GPIO34 = 1

#define EnableLATCHEN()       GpioDataRegs.GPASET.bit.GPIO22 = 1     /*锁存一次*/
#define DisableLATCHEN()      GpioDataRegs.GPACLEAR.bit.GPIO22 = 1

extern void DspInit(void);

#endif
