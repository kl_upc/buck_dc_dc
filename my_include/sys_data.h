#ifndef SYS_DATA_H_
#define SYS_DATA_H_
#include "DSP28x_Project.h"

typedef union
{
  float32  Float32Y;
  struct
    {
        Uint32  byte4           :8;
        Uint32  byte3           :8;
        Uint32  byte2           :8;
        Uint32  byte1           :8;
    }data_struct;
}CharToFloat;

typedef union
{
    int  value_buffer[6];
    struct
    {
        int  OutVolt;
        int  BusVoltPN;
        int  BusVoltON;
        int  DcCurrtO;
        int  Temperature1;
        int  Temperature2;
    }value;
}sys_run_value;

typedef union
{
    int  lmit_buffer[6];
    struct
    {
        int  OutVolt_Limit;
        int  BusVoltPN_Limit;
        int  BusVoltON_Limit;
        int  DcCurrtO_Limit;
        int  DcCurrtI_Limit;
        int  BusVol_Limit;
    }value;
}sys_limit_value;

typedef union
{
    Uint32  set_buffer[3];
    struct
    {
        Uint32  set_voltage;
        Uint32  set_current;
        Uint32  set_on_off;
    }value;
}sys_set_value;


typedef union
{
    Uint16  state_buffer;
    struct
    {
        Uint16  module_on_off               :1;
        Uint16  module_error                :1;
        Uint16  module_current_voltage      :1;
        Uint16  module_speed                :1;
        Uint16  module_over_voltagein       :1;
        Uint16  module_less_voltagein       :1;
        Uint16  module_over_voltageout      :1;
        Uint16  module_less_voltageout      :1;
        Uint16  module_over_current         :1;
        Uint16  module_over_temperature     :1;
        Uint16  module_set_on_off           :1;
        Uint16  reserve:5;
    }state;
}sys_run_state;

typedef union
{
    Uint32  id_buffer;
    struct
    {
        Uint32  address1    :1;
        Uint32  address2    :1;
        Uint32  address3    :1;
        Uint32  address4    :1;
        Uint32  address5    :1;
        Uint32  address6    :1;
        Uint32  address7    :1;
        Uint32  reserve     :25;
    }id;
}sys_id_value;

typedef struct
{
    Uint16  host_need_report_onff;
    Uint16  averge_current_report;
    Uint16  host_need_report_mode;
    Uint16  host_need_report_data;
}sys_can_state_value;

typedef struct
{
    int                     sys_emerg_fault;
    int                     sys_soft_fault;
//    Uint32                  sys_average_current[10];
    sys_run_state           sys_state;
    sys_set_value           sys_set;
    sys_run_value           sys_value;
    sys_limit_value         sys_limit;
    sys_id_value            sys_id;
    sys_can_state_value     sys_can_state;
}sys_run_parameter;

extern CharToFloat CanSendDateTemp1;
extern CharToFloat CanSendDateTemp2;
extern CharToFloat CanSendDateTemp10;
extern CharToFloat CanSendDateTemp20;
extern CharToFloat CanReDateTemp1;
extern CharToFloat CanReDateTemp2;
extern CharToFloat CanReDateTemp10;
extern CharToFloat CanReDateTemp20;

extern sys_run_parameter sys_run;
void GetSysId(void);
void SciComm(void);
void send485(Uint16 senddata);
extern Uint16 uart_r;
extern Uint16 send_fault;
#endif
