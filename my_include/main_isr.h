#ifndef MAIN_ISR_H_
#define MAIN_ISR_H_

#include "DSP28x_Project.h"


__interrupt void  main_isr(void);
__interrupt void  emerge_protect_isr(void);
#endif
